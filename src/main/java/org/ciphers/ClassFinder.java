package org.ciphers;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.*;
import java.util.*;
import java.util.stream.Stream;

class ClassFinder {

    /** Lists all classes found in given package
     * Created by acheron55
     * Found the code here: https://stackoverflow.com/a/28057735
     *
     * @param scannedPackage Package that is scanned.
     * @return List of all classes in given package
     * @throws IOException
     * @throws URISyntaxException
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public static List<Class<?>> find(String scannedPackage) throws IOException, URISyntaxException, ClassNotFoundException, InstantiationException, IllegalAccessException { //"Ciphers.Ciphers"

        List<Class<?>> classes = new ArrayList<>();
        URI uri = Objects.requireNonNull(ClassFinder.class.getResource("Ciphers")).toURI();
        Path myPath;

        final char PKG_SEPARATOR = '.';
        final String CLASS_FILE_SUFFIX = ".class";

        if (uri.getScheme().equals("jar")) {

            FileSystem fileSystem = FileSystems.newFileSystem(uri, Collections.<String, Object>emptyMap());

            //for Stream<Path> walk in jar file only, the '/' is needed as a separator, other instances, '.'
            myPath = fileSystem.getPath(scannedPackage.replace('.', '/'));

        } else
            myPath = Paths.get(uri);

        Stream<Path> walk = Files.walk(myPath, 1);
        for (Iterator<Path> it = walk.iterator(); it.hasNext();) {
            Path path = it.next();
            File file = new File(String.valueOf(path));
            FileFilter logFilefilter = p -> {
                if (p.getName().endsWith(CLASS_FILE_SUFFIX))
                    return true;
                return false;
            };

            if (logFilefilter.accept(file)) {
                String cipher = String.valueOf(path.getFileName()).replace(".class", "");
                classes.add(Class.forName(scannedPackage + PKG_SEPARATOR + cipher));

            }
        }
        return classes;
    }

    /**
     * Use a String list to match names of classes and filter these classes out of the class list.
     */
    public static List<Class<?>> filter(List<Class<?>> clazz, List<String> removeClasses) {
        List<Class<?>> classes = new ArrayList<>();

        for (Class<?> aClass : clazz) {
            boolean match = false;

            for (String removeClass : removeClasses) {

                if (aClass.getName().equals(removeClass))
                    match = true;
            }

            //If current class is not equal to one of the listed to-remove classes, add it.
            if (!match)
                classes.add(aClass);
        }
        return classes;
    }

    /** Prepend filename of jarFile
     * @return Filename without the extention.
     */
    public static String getSourceCodeName(){
        String jarName = new java.io.File(Main.class.getProtectionDomain()
                .getCodeSource()
                .getLocation()
                .getPath())
                .getName();
        if (jarName.indexOf(".") > 0)
            jarName = jarName.substring(0, jarName.lastIndexOf("."));
        return jarName;
    }
}
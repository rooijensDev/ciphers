package org.ciphers;

import java.lang.reflect.Method;

/**
 * All run iterations, containing the instance and which method was run within that instance.
 */
public class Iteration {
    Object instance;
    Method method;
}

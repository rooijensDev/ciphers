package org.ciphers;

import org.userio.Blocking;
import org.userio.Exceptions.InvalidScope;
import org.logger.Logger;

import java.io.IOException;
import java.net.URISyntaxException;

import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.List;

public class Menu {

	//All main cipher classes loaded in the JVM
	private final List<Class<?>> cipherClasses;

	/** Base 'Cipher' class, contains all info of each specific execution
	 * Save class and method that will be called to start a cipher process
	 */
	private Class<?> parentCipher;

	/** Sets input and output for cipher and prints iteration to CMD
	 * This method is called when either encrypt() or decrypt() is run.
	 *  This method sets the input so that it doesnt have to be set manually for each cipher's encrypt() and decrypt().
	 */
	private final Method parentMethod;

	//Iteration list, saves each method and instance in Iteration.class
	private List<Iteration> iterations = new ArrayList<>();

	/**
	 * Writes all cipher executable methods to fields, including class+method from parent cipher class to start any cipher.
	 * @throws NoSuchMethodException When a method isn't passed correctly to ClassFinder.filter()
	 */
	public Menu() throws NoSuchMethodException, IOException, URISyntaxException, ClassNotFoundException, InstantiationException, IllegalAccessException {

		cipherClasses = setCipherClasses();
		parentMethod = parentCipher.getDeclaredMethod("startCryption");
	}

	/**
	 * User handler when no cipher is being executed
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws InvalidScope
	 * @throws IOException Logger
	 */
	public void cipherMenu() throws InvocationTargetException, NoSuchMethodException, InstantiationException,
			IllegalAccessException, InvalidScope {

		boolean newIteration = true;

		while(newIteration) {

			executeCipher();

			System.out.print(System.lineSeparator());
			System.out.println("Would you like to perform another cipher?:");

			if (!(Blocking.getBool()))
				newIteration = false;
		}

		//Print all iterations
		Write.CMDWrite(iterations, parentCipher);

		//Optional File Write
		System.out.print(System.lineSeparator());
		System.out.println("Would you like to save your output?");

		if (Blocking.getBool())
			Write.FileWrite(iterations, parentCipher);
		else
			Logger.LOGGER.info("Skip writing all iterations to text-file");
	}

	/**
	 * Main method of the entire program, executes a user selected method
	 * Create iteration, set selected Cipher (class), get and run constructor, save instance.
	 * Then save selected Method.
	 * Then pass instance and method to the parent class for later call usage.
	 * Finally, call start() in parent class if an input is needed first, or call the selected Method.
	 * This pass is because:
	 * This way, the parent cipher can still get values from the child's class after the child's method is done,
	 * as it remembers which child class and method was executed.
	 * - If an input for a cipher must be set, the start() method in the parent class is called first in order to
	 * set the value in the parent field.
	 * This is only to guarantee an input will be set when needed.
	 * As every cipher has an input, the input handling is done in the parent class.
	 * Then, the parent class itself can invoke the child's encryption or decryption method afterwards.
	 * - If an input does not have to be set for the selected method, the method (in the child class) can just be
	 * invoked.
	 *
	 * @throws NoSuchMethodException During set of methodID and calling method
	 * @throws IllegalAccessException If modifier called in another class isn't public, non public methods won't be
	 * listed in the menu, thus this exception should never be thrown.
	 * @throws InvocationTargetException When the method or constructor invoked returns an error
	 * @throws InstantiationException When newInstance cannot be executed, unlisted classes shouldn't be accessible
	 * and thrown.
	 */
	private void executeCipher() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException,
			InstantiationException, InvalidScope {

		iterations.add(Iteration.class.newInstance());
		Logger.LOGGER.info("'START' Cipher iteration ( " + iterations.size() + " )");

		int cipherID = setSelectedCipher();
		int methodID;

		//Run constructor of specific cipher
		Constructor<?> constructor = cipherClasses.get(cipherID).getConstructor();
		iterations.get(getLatestIterationIndex()).instance = constructor.newInstance();

		// Set Information file if exists
		// Add printInformation() to menu
		Method setInformationFile = parentCipher.getDeclaredMethod("setInformationFile");
		setInformationFile.invoke(getLatestInstance());

		// Select method to run
		methodID = setSelectedMethod(getCipherMethods(cipherID));
		iterations.get(getLatestIterationIndex()).method = getCipherMethods(cipherID).get(methodID);

		// Set parameter of current iteration in the parent class.
		// Run method.
		Class<?>[] paramTypes = {Object.class, Method.class};
		Method setIteration = parentCipher.getDeclaredMethod("setIteration", paramTypes);
		setIteration.invoke(getLatestInstance(), getLatestInstance(), getLatestMethod());

		Logger.LOGGER.info("'START' Cipher: " +
				getLatestInstance().getClass().getSimpleName() + " - Action: " + getLatestMethod().getName());
		//If an input and output are required for the method, run start() in parent class first.
		//Else, skip start() and run the selected method in child class.
		if(		getLatestMethod().getName().equals("encrypt") ||
				getLatestMethod().getName().equals("decrypt"))
			parentMethod.invoke(getLatestInstance());
		else
			getLatestMethod().invoke(getLatestInstance());
	}

//	private void printCurrentIteration(){
//		System.out.print(System.lineSeparator());
//		System.out.println("instance: " + getLatestInstance());
//		System.out.println("Method:" + getLatestMethod());
//	}

//____________________________________________________________________________________________________________//
//											Setters and Getters
//											Non-User defined
//____________________________________________________________________________________________________________//

	/**
	 * Lists all classes found in the Ciphers package and filters Subclasses.
	 * Lists all .class files found in the Ciphers package (including the Subclasses package)
	 * and filter out all .class files that are not ciphers, being all .class files in the Subclasses package,
	 * Ciphers and CipherInterface. This leaves us only the .class files that are actual ciphers.
	 * Also defines the parentCipher (Ciphers.class), which is used to execute any public method of any cipher.
	 *
	 * @return All main Cipher classes, These classes are named after the cipher
	 */
	private List<Class<?>> setCipherClasses() throws IOException, URISyntaxException, ClassNotFoundException, InstantiationException, IllegalAccessException {

		//https://docs.oracle.com/javase/specs/jvms/se7/html/jvms-4.html#jvms-4.1-200-E.1
		final int privateStaticMethodValue = 10;
		final int interfaceMethodValue = 1536;
		final int privateMethodValue = 2;
		final int protectedMethodValue = 4;
		final int protectedStaticMethodvalue = 12;
		String PKG_PATH = Menu.class.getPackage().getName().replace('/', '.');
		final String CIPHER_PKG = "Ciphers";
		final char PKG_SEPARATOR = '.';

		List<Class<?>> allClasses = ClassFinder.find(PKG_PATH + PKG_SEPARATOR + CIPHER_PKG);
		List<String> unlistedClasses = new ArrayList<>();

		//Parent Cipher class
		unlistedClasses.add(PKG_PATH + PKG_SEPARATOR + CIPHER_PKG + PKG_SEPARATOR + "Ciphers");

		//Define parent class before filtering it out of the list that will consist of child ciphers only.
		for (Class<?> allClass : allClasses) {
			//If class is not public, remove
			if (allClass.getModifiers() != Modifier.PUBLIC)
				unlistedClasses.add(allClass.getName());

			//Instantiate parentCipher variable
			if (allClass.getName().equals(unlistedClasses.get(0)))
				parentCipher = allClass;

			if(allClass.getName().startsWith(PKG_PATH + PKG_SEPARATOR + CIPHER_PKG + PKG_SEPARATOR + "Subclasses."))
				unlistedClasses.add(allClass.getName());
		}
		return ClassFinder.filter(allClasses, unlistedClasses);
	}

	/**
	 * Add all public cipher methods to the list for user input
	 *
	 * @param selectedCipher To get the declared methods
	 * @return all methods the user can choose from
	 * @throws NoSuchMethodException Hardcoded string to match method name
	 */
	private List<Method> getCipherMethods(int selectedCipher) throws NoSuchMethodException, InvocationTargetException,
			IllegalAccessException {
		List<Method> methods = new ArrayList<>();

		//public methods that should not be accessible by the user
		final String[] unlistedMethods = {"writeAdditionalInfo", "initializeEncrypt", "initializeDecrypt"};

		//if information == null, don't add method
		Method info = parentCipher.getDeclaredMethod("hasInformation");
		if ((boolean) info.invoke(getLatestInstance()))
			methods.add(parentCipher.getDeclaredMethod("printInformation"));

		for (Method m : cipherClasses.get(selectedCipher).getDeclaredMethods()) {
			if (m.getModifiers() == Modifier.PUBLIC) {
				boolean unwanted = false;
				for (String unlistedMethod : unlistedMethods) {
					if ((m.getName().equals(unlistedMethod))) {
						unwanted = true;
						break;
					}
				}
				if (!unwanted)
					methods.add(m);
			}
		}

		return methods;
	}

	private int getLatestIterationIndex(){ return iterations.size()-1; }

	private Object getLatestInstance(){ return iterations.get(getLatestIterationIndex()).instance; }

	private Method getLatestMethod(){ return iterations.get(getLatestIterationIndex()).method; }

//	public Iteration getLatestIteration(){ return iterations.get(getLatestIterationIndex()); }

//____________________________________________________________________________________________________________//
//											Setters and Getters
//											User defined
//____________________________________________________________________________________________________________//

	/**
	 * Select cipher to execute from user input
	 * @return Zero-based Cipher ID
	 */
	private int setSelectedCipher() throws InvalidScope {
		System.out.print(System.lineSeparator());
		System.out.println("Which cipher would you like to execute:");

		for(int i=0; i<cipherClasses.size(); i++)
			System.out.println(i+1 + ": " + cipherClasses.get(i).getSimpleName());

		return Blocking.getIndex(1, cipherClasses.size()+1)-1;  //Keep it zero based after user interaction
	}

	/**
	 * Select method from selected cipher to execute from user input
	 * @param methods List of methods as options
	 * @return Zero-based Cipher Method ID
	 */
	private int setSelectedMethod(List<Method> methods) throws InvalidScope {
		System.out.print(System.lineSeparator());
		System.out.println("Which action would you like to perform on the cipher:");

		for(int i=0; i<methods.size(); i++)
			System.out.println(i+1 + ": " + methods.get(i).getName());

		return Blocking.getIndex(1,methods.size()+1)-1; //Keep it zero based after user interaction
	}
}
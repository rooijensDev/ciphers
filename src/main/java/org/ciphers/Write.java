package org.ciphers;

import org.logger.Logger;

import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

public class Write {

    public static void FileWrite(List<Iteration> iterations, Class<?> parentCipher) throws NoSuchMethodException {

        final Method writeCipherMaterial = parentCipher.getDeclaredMethod("setFileWriteMaterial");
        final String fileName = "ciphers-output.txt";

        try (PrintWriter out = new PrintWriter(fileName)) {
            Logger.LOGGER.info("Start writing all iterations to text-file");

            for (int currentInstance = 0; currentInstance < iterations.size(); currentInstance++) {
                //Main header to group executed methods is not needed if only one method is executed.
                if (iterations.size() > 1) {
                    if (currentInstance != 0)
                        out.print(System.lineSeparator());

                    out.println(writeHeader(currentInstance));
                }

                out.print("Cipher: ");
                if (iterations.get(currentInstance).method.getDeclaringClass().getSimpleName().equals("Ciphers"))
                    out.println(iterations.get(currentInstance).instance.getClass().getSimpleName());
                else
                    out.println(iterations.get(currentInstance).method.getDeclaringClass().getSimpleName());

                out.print("Action: ");
                //Ensure that the first letter of the method is always an uppercase.
                String methodInLowerCase = iterations.get(currentInstance).method.getName();
                out.println(methodInLowerCase.substring(0, 1).toUpperCase() + methodInLowerCase.substring(1));


                /* Add cipher information
                * If there is any information, add a subheader that is the length of the longest
                * string combination of class and method
                */
                List<String> FileWriteMaterial = (List<String>) writeCipherMaterial.
                        invoke(iterations.get(currentInstance).instance);
                if(FileWriteMaterial.size() != 0)
                    out.print(writeSubHeader(iterations.get(currentInstance).method));
                for (String s : FileWriteMaterial) out.println(s);

            }
            Logger.LOGGER.info("Finish writing all iterations to text-file");
            System.out.println("Successfully written to " + fileName);

        } catch (Exception e) {
            Logger.LOGGER.warning("Failed writing all iterations to text-file");
            System.err.println("Failed writing output");
        }
    }

    /**
     * Main header in the txt file, this groups each iteration
     * @param iteration Used to give each cipher a number
     * @return String for one textline, containing the header.
     */
    private static String writeHeader(int iteration) {
        String header = "";
        int headerLength = 20;

        header += "[";
        for (int i = 0; i < headerLength; i++)
            header += "_";

        header += ("Cipher " + (iteration + 1));

        for (int i = 0; i < headerLength; i++)
            header += "_";

        header += "]";
        return header;
    }


    /**
     * Sub header in the txt file, this splits the selected class + method from other printed information if available.
     *
     * @param method To determine which word is the longest
     * @return String for one textline, containing the header.
     */
    private static String writeSubHeader(Method method){
        String header = "";
        //Add line that equals longest line of header text
        int headerLength = Math.max(
                method.getName().length(),
                method.getDeclaringClass().getSimpleName().length()
        );

        //8 can be final here since "Cipher: " amd  "Action: " are the same length.
        for (int i = 0; i < 8 + headerLength; i++)
            header += "=";

        return header;
    }
    /**
     * Writes cipher iterations all at once after the user finished executing ciphers.
     *
     * Writes Cipher type, Method executed, Input and Output for each iteration.
     * @param iterations List of all iterations form Iteration.class, containing instance and executed method for each
     *                   entry.
     * @param parentCipher
     */
    public static void CMDWrite(List<Iteration> iterations, Class<?> parentCipher) throws InvocationTargetException,
            IllegalAccessException, NoSuchMethodException {
        Logger.LOGGER.info("Start printing all iterations to CMD");

        for (int i = 0; i < iterations.size(); i++) {
            if (i != 0)
                System.out.print(System.lineSeparator());
            System.out.println("Iteration: " + (i+1));
            System.out.println("Cipher: " +iterations.get(i).instance.getClass().getSimpleName());

            //Ensure that the first letter of the method is always an uppercase.
            String methodInLowerCase = iterations.get(i).method.getName();
            System.out.println("Method: " +
                    methodInLowerCase.substring(0, 1).toUpperCase() + methodInLowerCase.substring(1));

            /* If ran method returned a value (output), also print input and created output.
            // These values are saved in the parent class as every cipher contains these parameters.
            */
            if (iterations.get(i).method.getName().equals("encrypt") ||
                    iterations.get(i).method.getName().equals("decrypt")){
                char[] input = (char[]) parentCipher.getDeclaredMethod("getInput").
                        invoke(iterations.get(i).instance);
                char[] output = (char[]) parentCipher.getDeclaredMethod("getOutput").
                        invoke(iterations.get(i).instance);

                //2 spaces after input to match the other lines
                System.out.print("Input:  ");
                for (char c : input) System.out.print(c);
                System.out.print(System.lineSeparator());
                System.out.print("Output: ");
                for (char c : output) System.out.print(c);
                System.out.println("");
            }
        }
        Logger.LOGGER.info("Finish printing all iterations to CMD");
    }
}

package org.ciphers.Ciphers;

import java.util.*;

import org.ciphers.Ciphers.Subclasses.WriteTables;
import org.ciphers.Ciphers.Subclasses.ColumnarTransposition;
import org.ciphers.Ciphers.Subclasses.Substitution;
import org.ciphers.Ciphers.Subclasses.Polybius;
import org.ciphers.Ciphers.Subclasses.WriteHelpers;
import org.userio.Blocking;
import org.userio.Filter;

public class Adfgvx extends Ciphers implements CipherInterface {

	private char[] plainText;								//Filtered input
	private char[] cipherText;								//Final result value
	protected char[][]originalKeywords = new char[2][]; 	//Original keywords, saved for writing to text file
	private final char[][]keywords = new char[2][];			//Filtered keywords, used for encoding and decoding
															//Only allow letters in a char array

	private final int spaceInterval = 5;					//Spacing encrypted text for readability

	private final boolean isCaseSensitive = false;			//Remove all duplicate letters in keyword filtering
	private static final Map<Integer, Character> ADFGVX_LETTERS_MAP = new HashMap<>();
	private static final char[] ADFGVX_LETTERS = {'A', 'D', 'F', 'G', 'V', 'X'};

	private char[][] polybiusSquare;		//Table to read ADFGVX combinations
	private char[][] substitutionTable;		//Read polybius square table, 	linked to plaintext
	private char[][] transpositionTable;	//Columnar transposition, 		linked to ciphertext

	public Adfgvx(){
		setInformation("This is an Adfgvx cipher. http://practicalcryptography.com/ciphers/adfgvx-cipher/");
		ADFGVX_LETTERS_MAP.put(0,'A');
		ADFGVX_LETTERS_MAP.put(1,'D');
		ADFGVX_LETTERS_MAP.put(2,'F');
		ADFGVX_LETTERS_MAP.put(3,'G');
		ADFGVX_LETTERS_MAP.put(4,'V');
		ADFGVX_LETTERS_MAP.put(5,'X');
	}

	@Override
	public void initializeEncrypt() {
		//Set keywords
		boolean canBeEmpty = true;
		originalKeywords[0] = setKeyword(1, canBeEmpty);
		originalKeywords[1] = setKeyword(2, !canBeEmpty);
	}

	@Override
	public void initializeDecrypt() {
		//Set keywords
		boolean canBeEmpty = true;
		originalKeywords[0] = setKeyword(1, canBeEmpty);
		originalKeywords[1] = setKeyword(2, !canBeEmpty);

		//arrIn gets initialized in this loop and is met with conditions
		while(!isValidDecryptInput(input))
			input = Blocking.getString(false).toCharArray();
	}

	@Override
	public char[] encrypt() {
		boolean toUpper = true;

		plainText = Filter.capitalize(Filter.onlyLettersAndNumbers(input), toUpper);

		//Each plaintext letter returns 2 ciphertext (ADFGVX) letters
		cipherText = new char[plainText.length*2];

		sharedInitialisations();

		Substitution.setSubstitutionTable
				(substitutionTable, ADFGVX_LETTERS, ADFGVX_LETTERS, polybiusSquare, plainText);
		transpositionTable = ColumnarTransposition.encryptTransposition
				(substitutionTable, getAlphabeticalOrder(Filter.noDuplicates(keywords[1], isCaseSensitive)));
		cipherText = ColumnarTransposition.readTranspositionTable(transpositionTable, new char[cipherText.length]);

		return addSpaces(cipherText, spaceInterval);
	}

	@Override
	public char[] decrypt() {
		boolean toUpper = true;

		cipherText = Filter.capitalize(Filter.onlyLetters(input), toUpper);
		input = addSpaces(Filter.capitalize(Filter.onlyLetters(input), toUpper), spaceInterval);

		//Each 2 ciphertext (ADFGVX) Letters returns 1 plaintext letter.
		plainText = new char[cipherText.length/2];

		sharedInitialisations();

		ColumnarTransposition.setTranspositionTable
				(transpositionTable, Filter.noDuplicates(keywords[1], isCaseSensitive), cipherText);
		substitutionTable = ColumnarTransposition.decryptTransposition
				(transpositionTable, getAlphabeticalOrder(Filter.noDuplicates(keywords[1], isCaseSensitive)));
		plainText = Substitution.readSubstitutionTable(substitutionTable, ADFGVX_LETTERS_MAP, ADFGVX_LETTERS_MAP,
				polybiusSquare, plainText.length);

		return plainText;
	}

	/** Set keywords and fill in polybius square
	 *
	 */
	private void sharedInitialisations(){
		boolean toUpper = true;

		keywords[0] = Filter.capitalize(Filter.onlyLettersAndNumbers(originalKeywords[0]), toUpper);
		keywords[1] = Filter.capitalize(Filter.onlyLetters(originalKeywords[1]), toUpper);
		polybiusSquare = Polybius.polybiusSquare6x6(keywords[0]);

		setTableSizes(cipherText.length);
	}

	/** Set both table sizes of translateTable and transpositionTable
	 * Not 0 based
	 * The arrSize is the cipherText length for decrypt and the reserved cipherText length for encrypt.
	 * Both parameters have the same value, but are stored in their opposite variables when calling this function.
	 *
	 * @param arrSize Row size of translateTable & Col size of transposition table
	 */
	private void setTableSizes(int arrSize){
		int columns = Filter.noDuplicates(keywords[1], isCaseSensitive).length;
		int rows = (int) Math.ceil((double) arrSize / (double) columns);
		substitutionTable = new char[rows][columns];
		transpositionTable = new char[columns][rows];
	}

//____________________________________________________________________________________________________________//
//
//											Helper Methods
//
//____________________________________________________________________________________________________________//

	/** Check if conditions are met, re-writes arrIn by also getting a new user input for variable 'input'
	 *
	 * @return If every character given by the user is either an A,D,F,G,V or X.
	 * @param arr
	 */
	private boolean isValidDecryptInput(char[] arr) {
		arr = Filter.capitalize(Filter.onlyLetters(arr), true);
		if (arr.length % 2 != 0) {
			System.out.print(System.lineSeparator());
			System.err.println("The input length must be an even number. Please re-enter your input.");
			return false;
		}

		for (char c : arr) {
			boolean isValidLetter = false;

			for (Map.Entry<Integer, Character> entry : ADFGVX_LETTERS_MAP.entrySet()) {
				if (c == entry.getValue()) {
					isValidLetter = true;
					break;
				}
			}

			if (!isValidLetter) {
				System.out.print(System.lineSeparator());
				System.err.println("Not all letters are equal to A,D,F,G,V or X. Please re-enter your input.");
				return false;
			}
		}
		return true;
	}

	/** For every <INTERVAL> chars in an array, add a space
	 * These spaces are used to group the ciphertext in 5 for easier readability
	 *
	 * @param arr The array that needs spaces to be added
	 * @return a bit longer array with spaces in between
	 */
	private char[] addSpaces(char[] arr, int interval) {
		char[] out = new char
				[(int) ((double) arr.length + Math.ceil((double) arr.length/(double)interval) -1.0)];
		int selectedOutChar = 0;

		for (int i=0; i<arr.length; i++) {
			if (i % interval == 0 && !(i == 0)) {
				out[selectedOutChar] = ' ';
				selectedOutChar++;
			}
			out[selectedOutChar] = arr[i];
			selectedOutChar++;
		}
		return out;
	}

//____________________________________________________________________________________________________________//
//
//											All Write Methods
//
//____________________________________________________________________________________________________________//

	@Override
	public List<String> writeAdditionalInfo() {

		List<String> info = new ArrayList<>();
		info.addAll(WriteHelpers.writeKeywords(originalKeywords));
		info.addAll(WriteTables.dualHeaderTable(polybiusSquare, ADFGVX_LETTERS, ADFGVX_LETTERS,"Polybius"));

		int[] order = getAlphabeticalOrder(Filter.noDuplicates(keywords[1], isCaseSensitive));

		info.addAll(WriteTables.horizontalHeaderTable(substitutionTable, Filter.noDuplicates(keywords[1], isCaseSensitive), order, "Substitution"));
		info.addAll(WriteTables.verticalHeaderTable(transpositionTable, sortArr(Filter.noDuplicates(keywords[1], isCaseSensitive)), "Transposition"));
		return info;
	}
}
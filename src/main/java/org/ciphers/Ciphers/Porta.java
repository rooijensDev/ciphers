package org.ciphers.Ciphers;

import org.ciphers.Ciphers.Subclasses.Substitution;
import org.ciphers.Ciphers.Subclasses.WriteHelpers;
import org.ciphers.Ciphers.Subclasses.WriteTables;
import org.userio.Filter;

import java.util.ArrayList;
import java.util.List;

public class Porta extends Ciphers implements CipherInterface {

    private char[] plainText;               //Filtered input
    private char[] cipherText;              //Final result value
    protected char[] keyword;
    private final char[][] portaTable = createPortaTable();
    private char[][] substitutionTable;

    private final int numericOffset = 10;   //Numeric value of A = 10
    private final int yHeaderWidth = 2;     //{'A', 'B'}, {'C', 'D'} etc.
    private final int asciiOffset = 65;     //64 + zero based, Used to convert the column interger to a capitalized char.
                                            // (0 + asciiOffset -> 'A')
    public Porta() {setInformation("This is a Porta cipher. http://practicalcryptography.com/ciphers/porta-cipher/");}

    @Override
    public void initializeEncrypt() {
        keyword = setKeyword(false);
    }

    @Override
    public void initializeDecrypt() {
        keyword = setKeyword(false);
    }

    @Override
    public char[] encrypt() {
        plainText = Filter.onlyLetters(input);
        cipherText = new char[plainText.length];
        keyword = Filter.onlyLetters(keyword);

        substitutionTable = Substitution.setSimpleSubstitutionTable(plainText, keyword, false);

        for (int emptyElement = 0; emptyElement < cipherText.length; emptyElement++) {

            //Both characters are capitalized
            int row = (int) Math.floor((
                    (double) Character.getNumericValue(substitutionTable[0][emptyElement]) - numericOffset)
                            / yHeaderWidth);

            //Both characters are not capitalized
            int col = Character.getNumericValue(substitutionTable[1][emptyElement]) - numericOffset;

            cipherText[emptyElement] = Character.toUpperCase(portaTable[row][col]);
        }

        return cipherText;
    }

    @Override
    public char[] decrypt() {
        cipherText = Filter.onlyLetters(input);
        plainText = new char[cipherText.length];
        keyword = Filter.onlyLetters(keyword);

        substitutionTable = Substitution.setSimpleSubstitutionTable(cipherText, keyword, false);

        for (int emptyElement = 0; emptyElement < plainText.length; emptyElement++) {

            //Both characters are capitalized
            int row = (int) Math.floor((
                    (double) Character.getNumericValue(substitutionTable[0][emptyElement]) - numericOffset)
                            / yHeaderWidth);

            //Both characters are not capitalized
            for (int col = 0; col < portaTable[0].length; col++) {
                if (Character.toUpperCase(portaTable[row][col]) == Character.toUpperCase(cipherText[emptyElement])) {
                    plainText[emptyElement] = (char) (col + asciiOffset);
                    break;
                }
            }
        }

        return plainText;
    }

    private char[][] createPortaTable() {
        char[][] table = new char[13][26];
        String alphabet = "abcdefghijklmnopqrstuvwxyz";

        //First half
        for (int row = 0; row < 13; row++) {
            for (int col = 0; col < 13; col++) {

                int letterIndex = (alphabet.length() / 2 + row + col) % 26;
                if (letterIndex < alphabet.length() / 2)
                    letterIndex += alphabet.length() / 2;

                table[row][col] = Character.toUpperCase(alphabet.charAt(letterIndex));
            }
        }

        //Second half
        for (int row = 12; row >= 0; row--) {
            for (int col = 0; col < 13; col++) {

                int letterIndex = (13 - row + col) % 26;
                if (letterIndex >= alphabet.length() / 2)
                    letterIndex -= alphabet.length() / 2;

                table[row][col + 13] = Character.toUpperCase(alphabet.charAt(letterIndex));
            }
        }
        return table;
    }

    @Override
    public List<String> writeAdditionalInfo() {
        List<String> info = new ArrayList<>();
        char[] xHeader = "abcdefghijklmnopqrstuvwxyz".toCharArray();

        char[][] yHeader = new char[xHeader.length/2][2];
        for(int row = 0; row < yHeader.length; row++)
            for (int col = 0; col < yHeader[0].length; col++)
                yHeader[row][col] = (char)(row * 2 + col + asciiOffset);

        info.addAll(WriteHelpers.writeKeyword(keyword));
        info.addAll(WriteTables.dualHeaderTable(portaTable, xHeader, yHeader, "Porta"));
        info.addAll(WriteTables.basicTable(substitutionTable, "Substitution", true));

        return info;
    }
}

package org.ciphers.Ciphers;

import org.userio.Blocking;

import java.util.ArrayList;
import java.util.List;

import org.ciphers.Ciphers.Subclasses.WriteHelpers;
import org.ciphers.Ciphers.Subclasses.WriteTables;
import static org.ciphers.Ciphers.Subclasses.Shift.shiftLetters;

public class Caesar extends Ciphers implements CipherInterface {

	protected int shift;

	@Override
	public void initializeEncrypt() {
		shift = setKey();
	}

	@Override
	public void initializeDecrypt() {
		if (!isShiftKnown())
			printAllShifts();

		shift = setKey();
	}

	@Override
	public char[] encrypt() {
		return shiftLetters(input, shift);
	}
	@Override
	public char[] decrypt() {

		return shiftLetters(input, shift);
	}

	private boolean isShiftKnown() {
		System.out.println("Is the decrypt shift known? (-1 * original shift)");
		return Blocking.getBool();
	}

	/** Shows every shift output possibility to the end-user.
	 * Visually represents a valid output in a list of shifts with the shift number.
	 */
	private void printAllShifts(){

		for (int shiftCount = -12; shiftCount <= 13; shiftCount++) {

			if (shiftCount == -1 || shiftCount == 1)
				System.out.print(shiftCount + " Shift: ");
			else
				System.out.print(shiftCount + " Shifts: ");

			System.out.println(shiftLetters(input, shiftCount));
		}
		System.out.print(System.lineSeparator());
	}

	@Override
	public List<String> writeAdditionalInfo() {
		List<String> list = new ArrayList<>();
		list.addAll(WriteHelpers.writeInteger("Shift", shift));

		char[][] table = new char[2][];
		table[0] = shiftLetters(input, 0);
		table[1] = shiftLetters(input, shift);
		list.addAll(WriteTables.basicTable(table, "Caesar", true));
		return list;
	}
}
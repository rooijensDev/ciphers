package org.ciphers.Ciphers;

import java.util.List;

public class Rot13 extends Ciphers implements CipherInterface{

    @Override
    public void initializeEncrypt() {}

    @Override
    public void initializeDecrypt() {}

    @Override
    public char[] encrypt() {
        return shiftLetters();
    }

    @Override
    public char[] decrypt() {
        return shiftLetters();
    }

    @Override
    public List<String> writeAdditionalInfo() {
        return null;
    }

    private char[] shiftLetters(){
        char[] arr = new char[input.length];

        for (int i = 0; i < input.length; i++) {

            if (Character.isLetter(input[i])) {
                int originalAlphabetPosition = input[i] - 'a';
                int newAlphabetPosition = (originalAlphabetPosition + 13) % 26;
                arr[i] = (char) ('a' + newAlphabetPosition);

            } else {
                arr[i] = input[i];

            }
        }
        return arr;
    }
}
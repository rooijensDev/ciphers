package org.ciphers.Ciphers.Subclasses;

import org.ciphers.Ciphers.Ciphers;

/** Encrypt, Decrypt, set, read and write ColumnarTransposition Tables
 *
 * Encrypt:
 * Step 1: A Columnar Transposition changes the order of the columns in alphabetical order based on the keyword.
 * The keyword must not contain any duplicate letters, the columns must be unique to sort them accordingly.
 * Step 2: The message is then read vertically instead of horizontally to get the output.
 *
 * Example (keyword Tray, message fortification):
 *
 * Step 1:
 * |TRAY|
 * |2103| The keyword with their correlating number in alphabetical order
 * |----|
 * |FORT|
 * |IFIC|
 * |ATIO|
 * |N   |
 * |----|
 *
 * Step 2:
 * |ARTY|
 * |0123|
 * |----|
 * |ROFT|
 * |IFIC|
 * |ITAO|
 * |  N |
 * |----|
 *
 * This table is set flipped for easier readability:
 *
 * =------
 * A|RII |
 * R|OFT |
 * T|FIAN|
 * Y|TCO |
 *=-------
 *
 * Output: RIIOFCFIANTCO
 *
 */
public class ColumnarTransposition {

    /** Give an empty table with a set size and fill it in based on the given keyword and input text.
     *
     * This method is used if the text within the method is already known.
     * Table size - input text determines the amount of empty elements in the table.
     * This amount x is linked to the last x letters of the keyword.
     * After sorting the rows in alphabetical order based on the keyword, the input text is filled in
     * row after row, skipping rows where the keyword letters match x.
     *
     * Example:
     * Keyword: kruidenboter -> kruidenbot (filter duplicates)
     * Input: AFAGV DDXDX XGDFD AAGFA AAGVA DADDX GAGFV G
     *
     * table size - input length = 4. So 4 empty spots exist.
     * kruideNBOT = last 4 letters.
     * row numbers match the keyword in alphabetical order.
     * Fill in the keyword row by row (left to right) and skip the last column for the letters NBOT.
     *
     * =---------
     * B|A F A  |
     * D|G V D D|
     * E|X D X X|
     * I|G D F D|
     * K|A A G F|
     * N|A A A  |
     * O|G V A  |
     * R|D A D D|
     * T|X G A  |
     * U|G F V G|
     * =---------
     *
     * @param table Empty transposition table with set size
     * @param keyword Keyword used to define the order
     * @param cipherText Input text that will be filled into the table
     * @return A filled in transposition table
     */
    public static char[][] setTranspositionTable(char[][] table, char[] keyword, char[] cipherText){
        int[] order = Ciphers.getAlphabeticalOrder(keyword);
        int amountOfEmptyElements = table.length * table[0].length - cipherText.length;
        int contentCount = 0;
        boolean added = false;

        for (int row = 0; row < table.length; row++) {
            for (int col = 0; col < table[0].length; col++) {
                if (col == table[0].length -1) {
                    for (int orderCount = keyword.length - amountOfEmptyElements;
                         orderCount < order.length; orderCount++) {
                        if (order[orderCount] == row){
                            table[row][col] = ' ';
                            added = true;
                            break;
                        }
                    }
                }

                if (!added) {
                    table[row][col] = cipherText[contentCount];
                    contentCount++;
                }
                added = false;
            }
        }

        return table;
    }

    /** Traverses row by row and adding them to a transpositioned column through row column
     *
     * Differs from encryptTransposition() by using alphabetical order to select different rows per column.
     * encryptTransposition() locks to a column and traverses through each row in order. This method jumps
     * through the rows each column.
     *
     * @param inputTable Filled in table that will be transpositioned
     * @param order non sorted alphabetical order of the key (Ex: Tray = 2103) Filter duplicates
     * @return Transpositioned table
     */
    public static char[][] decryptTransposition(char[][] inputTable, int[] order){
        char[][] table = new char[inputTable[0].length][inputTable.length]; //Flipped row and col for new table
        for (int row = 0; row < table.length; row++) {
            for (int col = 0; col < table[0].length; col++) {
                table[row][col] = inputTable[order[col]][row];
            }
        }

        return table;
    }

    /** Traverses column through column and adding them to a transpositioned row through row table
     *
     * Differs from decryptTransposition() by locking to a column and traverses through each row in order.
     * decryptTransposition() uses alphabetical order to jump through the column each row, not in row order.
     * through the rows each column.
     *
     * @param inputTable table that will be transpositioned (original rows and columns!)
     * @param order Alphabetical order of the key
     * @return Transpositioned table
     */
    public static char[][] encryptTransposition(char[][] inputTable, int[] order) {
        char[][] outputTable = new char[inputTable[0].length][inputTable.length]; //Flip rows & columns to transposition
        //The column mustn't change for each row traversion.
        int selectedCol = 0;

        for (int row = 0; row < outputTable.length; row++) {
            for (int k=0; k<order.length; k++)
                if (order[k] == row) {
                    selectedCol = k;
                    break;
                }
            for (int col = 0; col < outputTable[0].length; col++) {
                outputTable[row][col] = inputTable[col][selectedCol];
            }
        }
        return outputTable;
    }

    /** Reads 2D table and returns 1D
     *
     * @param transpositionTable The table as input
     * @param outputMessage The size of the message which will be returned
     * @return the encrypted message which is double the used elements of the table size
     */
    public static char[] readTranspositionTable(char[][] transpositionTable, char[] outputMessage) {
        //2D to 1D array
        int pointer = 0;

        for (int i=0; i<transpositionTable.length; i++)
            for (int j=0; j<transpositionTable[0].length; j++){
                if (!(transpositionTable[i][j] == ' ')) {
                    outputMessage[pointer] = transpositionTable[i][j];
                    pointer++;
                }
            }
        return outputMessage;
    }
}
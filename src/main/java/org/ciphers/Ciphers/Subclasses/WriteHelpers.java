package org.ciphers.Ciphers.Subclasses;

import java.util.ArrayList;
import java.util.List;

public class WriteHelpers {

    /** Writes a keyword with its header to textfile
     * Call this function using String x.add()
     *
     * @param keyword Keyword to be printed: char[]
     * @return Keyword with its headers as string.
     */
    private static String writeKeywordHelper(char[] keyword){
        StringBuilder buffer = new StringBuilder();
        for (char c : keyword)
            buffer.append(c);
        return buffer.toString();
    }

    /** Writes multiple keywords with its headers to textfile
     * Call this function using List<String> x.addAll()
     *
     * @param keywords Keywords to be printed: char[][]
     * @return Keywords with its headers as string list.
     */
    public static List<String> writeKeywords(char[][] keywords){
        List<String> words = new ArrayList<>();

        words.add("");
        for(int i = 0; i < keywords.length; i++) {
            words.add("Keyword " + (i+1) +":");
            words.add(writeKeywordHelper(keywords[i]));
            if(i < keywords.length -1)
                words.add("");
        }
        return words;
    }

    public static List<String> writeKeyword(char[] keyword){
        List<String> words = new ArrayList<>();
        words.add("");
        words.add("Keyword: ");
        words.add(writeKeywordHelper(keyword));
        return words;
    }

    public static List<String> writeInteger(String name, int integer){
        List<String> list = new ArrayList<>();
        list.add("");
        list.add(name + ": ");
        list.add(String.valueOf(integer));
        return list;
    }
}

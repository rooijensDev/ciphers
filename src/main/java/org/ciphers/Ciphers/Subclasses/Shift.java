package org.ciphers.Ciphers.Subclasses;

import org.logger.Logger;

public class Shift {

    /** Shifts letters only.
     *
     * Capitalization remains the same.
     * Numbers, symbols and characters are not shifted but also not excluded from the return.
     * @param input The char array that will be used to create the shifted result.
     * @param shift The amount of shifts.
     * @return
     */
    public static char[] shiftLetters(char[] input, int shift) {
        char[] arr = new char[input.length];
        shift %= 26;

        try {
            for (int i = 0; i < input.length; i++) {

                if (Character.isLetter(input[i])) {
                    int numericShift = Character.getNumericValue(input[i]) + shift;

                    if (numericShift > Character.getNumericValue('z'))
                        arr[i] = (char) (input[i] + shift - 26);

                    else if (numericShift < Character.getNumericValue('a'))
                        arr[i] = (char) (input[i] + shift + 26);

                    else
                        arr[i] = (char) (input[i] + shift);

                } else
                    arr[i] = input[i];
            }

        } catch (Exception e) {
            Logger.LOGGER.warning("Shifting error");
            System.err.println("Something went wrong when shifting a Caesar cipher");
        }
        return arr;
    }
}

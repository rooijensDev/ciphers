package org.ciphers.Ciphers.Subclasses;

import java.util.Map;

public class Substitution{

    public static char[][] setSimpleSubstitutionTable(char[] input, char[] keyword, boolean capitalizeBottomRow) {
        //TODO warn user if keyword > input
        char[][] table = new char[2][input.length];

        //Upper keyword row
        for (int i = 0; i < table[0].length; i++)
            table[0][i] = Character.toUpperCase(keyword[i % keyword.length]);

        //Lower array row
        for (int i = 0; i < table[1].length; i++)
            if (capitalizeBottomRow)
                table[1][i] = Character.toUpperCase(input[i]);
            else
                table[1][i] = Character.toLowerCase(input[i]);

        return table;
    }

    /** Uses a polybius square to fill in a larger table
     *
     * Each row and column has a character assigned to it, being the rowKeyword and the columnKeyword (0 based).
     * This table replaces each plaintext char with the columnword and rowword char.
     * Making the table fit twice the size of the plaintext.
     *
     * Example (columnKeyword Hello!, rowKeyword World!, message test):
     * Polybius Square Example:
     *  |W O R L D !|
     * -|-----------|
     * H|A 1 B 2 C 3|
     * E|D 4 E 5 F 6|
     * L|G 7 H 8 I 9|
     * L|J 0 K L M N|
     * O|O P Q R S T|
     * !|U V W X Y Z|
     * -|-----------|
     *
     * TEST
     * T = row 4 column 5 -> row O column ! -> output: O!
     * E = row 2 colunm 1 -> row E column R -> output: ER
     * S = row 4 column 4 -> row O column D -> output: OD
     * T = row 4 column 5 -> row O column ! -> output: O!
     *
     * Output: O!ERODO!
     * Input = 4 chars, Output = 8 chars
     *
     * Step 1: For each plainText char, find its location in the polybius square and select it.
     * Step 2: Select the char ID of the COLUMN keyword that matches the COLUMN ID of the selected char.
     * Step 3: Add char to table (row by row, column by column)
     * Step 4: Select the char Id of the ROW keyword that matches the ROW ID of the selected char.
     * Step 5: Add char to table (row by row, column by column)
     *
     * @param table Empty Substitution table with set size
     * @param rowKeyword Each char corresponds to a row number (word is written vertically)
     * @param columnKeyword Each char corresponds to a column number (word is written horizontally)
     * @param polybiusSquare Table used to match plaintext char
     * @param plainText Input text used to read polybius Square
     * @return A filled in transposition table
     */
    public static char[][] setSubstitutionTable(char[][] table, char[] rowKeyword, char[] columnKeyword,
                                                char[][] polybiusSquare, char[] plainText) {

        //Traverses through the input char array to link each char to the keyword table
        int selectedInput = 0;

        //Each letter corresponds to 2 AFDGVX letters, letter 2 is for the next iteration
        char letter1 = ' ';
        char letter2 = ' ';

        //When a letter is matched to a spot in table1
        boolean match = false;

        /*Fill table
         * - The first double for loop points to the next free spot of the translate table.
         * - The second double for loop links each input char to a spot in the polybius square, this spot
         * corresponds to 2 ADFGVX letters (the row and col number)
         */
        for (int substitutionRow = 0; substitutionRow < table.length; substitutionRow++) {
            for (int substitutionCol = 0; substitutionCol < table[0].length; substitutionCol++) {

                //Adds the second ADFGVX letter (Only 1 char can be added in each for loop iteration
                if (match) {
                    table[substitutionRow][substitutionCol] = letter2;
                    match = false;
                    continue;
                }

                //Find match in keyword table
                //If there are letters left to be added
                if (selectedInput < plainText.length) {
                    for (int polybiusRow = 0; polybiusRow < polybiusSquare.length; polybiusRow++) {
                        for (int polybiusCol = 0; polybiusCol < polybiusSquare[0].length; polybiusCol++) {
                            if (plainText[selectedInput] == polybiusSquare[polybiusRow][polybiusCol]) {
                                match = true;

                                //Column is vertical, to select the letter, you must count in rows (vise versa)
                                letter1 = rowKeyword[polybiusRow];
                                letter2 = columnKeyword[polybiusCol];
                                break;
                            }
                        }
                        if (match)
                            break;
                    }
                    //Add first letter to table, add second letter with the new iteration
                    table[substitutionRow][substitutionCol] = letter1;
                    selectedInput++;
                } else
                    //Fill the unused spots so that it can be compared to and skipped when
                    // performing a transposition on the table
                    table[substitutionRow][substitutionCol] = ' ';
            }
        }
        return table;
    }

    /** Matches substitution text to the polybius square keywords and returns the polybius chars.
     *
     * Reads the substitution table row by row, column by column.
     * For each 2 consecutive elements (1st element is row, 2nd element is column of polybius square),
     * select the polybius char that matches row and column. (keywords can not have duplicates)
     * This turns a 2D substitution table into a 1D char array.
     *
     * Example (columnKeyword Hello!, rowKeyword World!, message testing):
     * Polybius Square Example:
     *   A D F G V X
     * -|-----------|
     * A|A 1 B 2 C 3|
     * D|D 4 E 5 F 6|
     * F|G 7 H 8 I 9|
     * G|J 0 K L M N|
     * V|O P Q R S T|
     * X|U V W X Y Z|
     * -|-----------|
     *
     * Substitution table:
     * =T R A Y=
     * =2 1 0 3=
     * =========
     * |V X D F|
     * |V V V X|
     * |F V G X|
     * |F A    |
     * ---------
     *
     * Output: T  E  S  T  I  N  G
     *         VX DF VV VX FV GX FA
     *
     * @param table Filled in Substitution table
     * @param rowKeyword Each char corresponds to a row number in the polybius square (word is written vertically)
     *                   Int = row ID
     * @param columnKeyword Each char corresponds to a column number  in the polybius square
     *                      (word is written horizontally) Int = column ID
     * @param polybiusSquare Table used to match cipherText (information in the substitution table)
     * @param outputLength The length of the char[] you will be returning
     * @return An array read from polybius square, using the row and column keywords as row and column indicators.
     */
    public static char[] readSubstitutionTable(char[][]table, Map<Integer, Character> rowKeyword,
                                               Map<Integer, Character> columnKeyword,
                                               char[][] polybiusSquare, int outputLength){
        char[] out = new char[outputLength];

        //Points to a free spot in the output array
        int outPointer = 0;

        //Row and column pointer
        int[] loc = new int[2];
        //Ensures that each exchange saves location is a new grouped location.
        int locCount = 0;

        for (int row = 0; row < table.length; row++) {
            for (int col = 0; col < table[0].length; col++) {
                if (table[row][col] == ' ')
                    return out;
                for (Map.Entry<Integer, Character> entry : rowKeyword.entrySet()) {
                    if (entry.getValue() == table[row][col]){
                        loc[locCount % 2] = entry.getKey();
                        locCount++;
                        break;
                    }

                }
                if (locCount % 2 == 0) {
                    if (outPointer <= outputLength - 1) {
                        out[outPointer] = polybiusSquare[loc[0]][loc[1]];
                        outPointer++;
                    }
                }
            }
        }
        return out;
    }
}
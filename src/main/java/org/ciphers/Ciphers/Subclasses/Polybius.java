package org.ciphers.Ciphers.Subclasses;

import org.userio.Filter;

public class Polybius {

    /** Fill 6x6 square with all 26 letters and 10 digits.
     * Traverse through rows -> cols. Fill in keyword (no duplicate chars).
     * Add digit after letter if digit has not been used yet (A=1..I=9, J=0).
     *
     * @param keyword Keyword to make the square unique. Keyword filtering for validity is done within the method.
     * @return The polybius square.
     */
    public static char[][] polybiusSquare6x6(char[] keyword) {

        boolean upperCase = true;

        keyword = Filter.capitalize(
                        Filter.noDuplicates(keyword, false),
                upperCase);

        char[][] polybiusSquare = new char[6][6];

        //Traverse through table and point to free spot, fills with alphabet after keyword is used
        int selectedPolybiusChar = 0;
        int selectedLeftoverChar = 0;

        //Skips an iteration to add coherent number
        boolean addNumber = false;

        //Used to filter out used characters from keyword
        String leftOvers = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

        //Fill the 2D array
        for (int rows = 0; rows < polybiusSquare.length; rows++) {
            for (int cols = 0; cols < polybiusSquare.length; cols++) {

                //If number needs to be added, do so and skip this iteration to move to a new spot
                if (addNumber) {
                    //Get letter of previous spot to determine coherent number
                    int prevCol;
                    int prevRow;
                    int numericValueOffset;

                    //Select position of previous spot
                    if (cols != 0) {
                        prevRow = rows;
                        prevCol = cols - 1;
                    } else {
                        prevRow = rows - 1;
                        prevCol = polybiusSquare.length - 1;
                    }

                    //for output: A == 1, B == 2 ... I == 9, J == 0 (J != 10)
                    //If J, then value = 0
                    int selectedLetter = Character.getNumericValue(polybiusSquare[prevRow][prevCol]);
                    if (selectedLetter == 19)
                        selectedLetter = 0;
                    else
                        selectedLetter -= 9;

                    //+48 for ASCII Values, add digit to new spot that is coherent to char of previous spot
                    polybiusSquare[rows][cols] = (char) (48 + selectedLetter);

                    leftOvers = leftOvers.replace
                            (String.valueOf(selectedLetter), "");

                    //This spot is filled, we can go to the next free spot
                    addNumber = false;
                    continue;
                }

                //If keyword not fully used
                if (selectedPolybiusChar < keyword.length) {
                    polybiusSquare[rows][cols] = keyword[selectedPolybiusChar];

                    //Filter letter out of left-over letters for this else statement
                    leftOvers = leftOvers.replace
                            (String.valueOf(keyword[selectedPolybiusChar]), "");

                    selectedPolybiusChar++;

                    //Add the rest of alphabet
                } else {
                    polybiusSquare[rows][cols] = leftOvers.charAt(selectedLeftoverChar);
                    selectedLeftoverChar++;
                }

                int numericValueOfLetter = Character.getNumericValue(polybiusSquare[rows][cols]);

                //If character that was just added >= 'A' && <= J, add its coherent number next iteration
                if (numericValueOfLetter >= 10 && numericValueOfLetter <= 18)
                    if (leftOvers.contains(String.valueOf(numericValueOfLetter - 9)))
                        addNumber = true;

                if (numericValueOfLetter == 19)
                    if (leftOvers.contains(String.valueOf(0)))
                        addNumber = true;
            }
        }
        return polybiusSquare;
    }

    /** Fill 5x5 square with 25 letters.
     * Traverse through rows -> cols. Fill in keyword (no duplicate chars).
     * By default, I+J are combined (both return I).
     *
     * @param keyword Keyword to make the square unique. Keyword filtering for validity is done within the method.
     * @return The polybius square.
     */
    public static char[][] polybiusSquare5x5(char[] keyword) {
        char letterRemoved = 'J';
        char letterReplacement = 'I';

        return polybiusSquare5x5(keyword, letterRemoved, letterReplacement);
    }

    /** Fill 5x5 square with 25 letters.
     * Traverse through rows -> cols. Fill in keyword (no duplicate chars).
     * Customize the letter which will be removed and what letter it will be replaced by.
     *
     * @param keyword Keyword to make the square unique. Keyword filtering for validity is done within the method.
     * @return The polybius square.
     */
    public static char[][] polybiusSquare5x5(char[] keyword, char letterRemoved,  char letterReplacement) {

        boolean upperCase = true;

        keyword = Filter.onlyLetters(
                    Filter.capitalize(
                        Filter.noDuplicates(
                                keyword, false),
                        upperCase)
        );

        char[][] polybiusSquare = new char[5][5];

        //Traverse through table and point to free spot, fills with alphabet after keyword is used
        int selectedPolybiusChar = 0;
        int selectedLeftoverChar = 0;


        //Used to filter out used characters from keyword
        String leftOvers = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        leftOvers = leftOvers.replace(String.valueOf(letterRemoved), String.valueOf(letterReplacement));
        //Fill the 2D array
        for (int rows = 0; rows < polybiusSquare.length; rows++) {
            for (int cols = 0; cols < polybiusSquare.length; cols++) {
                char currentChar;

                //If keyword not fully used
                if (selectedPolybiusChar < keyword.length) {
                    if (keyword[selectedPolybiusChar] == letterRemoved) {
                        currentChar = letterReplacement;
                        selectedPolybiusChar++;

                    } else {
                        currentChar = keyword[selectedPolybiusChar];
                        selectedPolybiusChar++;
                    }

                    polybiusSquare[rows][cols] = currentChar;

                    //Filter letter out of left-over letters for this else statement
                    leftOvers = leftOvers.replace
                            (String.valueOf(currentChar), "");

                //Add the rest of alphabet
                } else {
                    currentChar = leftOvers.charAt(selectedLeftoverChar);

                    if(currentChar == letterReplacement) {
                        polybiusSquare[rows][cols] = currentChar;
                        leftOvers = leftOvers.replace
                                (String.valueOf(currentChar), "");


                    } else {
                        polybiusSquare[rows][cols] = currentChar;
                        selectedLeftoverChar++;
                    }


                }
            }
        }
        return polybiusSquare;
    }
}
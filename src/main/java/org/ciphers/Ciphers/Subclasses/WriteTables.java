package org.ciphers.Ciphers.Subclasses;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

public class WriteTables {

    private static int horizontalSpacing = 1;
    private static int verticalSpacing = 0;

    /** Set spacing between elements in a table
     *
     * @param horizontal standard 1 spacing, number is equal to spaces in between elements
     * @param vertical standard 0 spacing, number is equal to whitelines between rows
     */
    public static void setSpacing(int horizontal, int vertical){
        horizontalSpacing = horizontal;
        verticalSpacing = vertical;
    }

    private static int getDigitCount(int digit){
        int count;
        if (digit == 0)
            count = 1;
        else
             count = (int)(Math.log10(digit)+1);
        return count;
    }

    private static int getMaxDigitLength(int[] numbers){
        int[] numbersSorted = Arrays.copyOf(numbers, numbers.length);
        Arrays.sort(numbersSorted);
        int maxDigitLength = getDigitCount(numbersSorted[numbersSorted.length-1]); //amount of spaces == digit amount of largest number

        return maxDigitLength;
    }

    private static int getMaxDigitLength(int[][] numbers){
        int maxDigitLength = 0;

        for(int[] row: numbers){
            int[] numbersSorted = Arrays.copyOf(row, row.length);
            Arrays.sort(numbersSorted);
            int lastDigit = numbersSorted[numbersSorted.length -1];

            int currentLength = getDigitCount(lastDigit);
            if(currentLength > maxDigitLength)
                maxDigitLength = currentLength;
        }
        return maxDigitLength;
    }

//____________________________________________________________________________________________________________//
//
//											All Building Methods
//
//____________________________________________________________________________________________________________//

    /** Stores all elements with spacing in a String
     *
     * @param tableRow Elements with no spaces
     * @return Elements with spaces as String
     */
    private static StringBuilder buildRow(char[] tableRow){
        StringBuilder buffer = new StringBuilder();
        for (int i = 0; i < tableRow.length; i++) {
            buffer.append(tableRow[i]);
            if (i < tableRow.length -1)
                for (int space = 0; space < horizontalSpacing; space++)
                    buffer.append(" ");
        }

        return buffer;
    }

    /** A Row full of spaces that matches the length of the table rows
     *
     * @param rowLength A String with the size of rowlength + spaces in between
     * @return A String of spaces
     */
    private static StringBuilder buildEmptyRow(int rowLength){
        StringBuilder buffer = new StringBuilder();
        for (int i = 0; i < rowLength; i++) {
            buffer.append(" ");
            if (i < rowLength -1)
                for (int space = 0; space < horizontalSpacing; space++)
                    buffer.append(" ");
        }

        return buffer;
    }

    /** Builds a String of numbers with dynamic spacing
     *
     * If a number is more than one digit, spacing will be reduced by the excess digit amount to match other rows
     *
     * @param numberRow All integers
     * @return A String of integers with dynamic spacing
     */
    private static StringBuilder buildRow(int[] numberRow){
        StringBuilder buffer = new StringBuilder();
        for (int i = 0; i < numberRow.length; i++) {
            buffer.append(numberRow[i]);
            int digitCount = getDigitCount(numberRow[i]);

            if (i < numberRow.length -1)
                for (int space = 0; space < horizontalSpacing - digitCount + 1; space++)
                    buffer.append(" ");
        }

        return buffer;
    }

    /** For tables with a vertical header: Builds a horizontal line.
     *
     * The division line has a different marker when it matches the same column as the vertical header.
     *
     * @param tableWidth The length of the table (excluding spacing)
     * @param yHeaderWidth The vertical length of the header (excluding spacing)
     * @param invertDash (0 = '-' first, '=' second, 1 = '=' first, '-' second)
     * @return A division line that matches the size of the table including spacing and the vertical header
     */
    private static String buildLineRow(int tableWidth, int yHeaderWidth, boolean invertDash){
        StringBuilder buffer = new StringBuilder();

        for (int i = 0; i < yHeaderWidth + ((yHeaderWidth - 1) * horizontalSpacing); i++) {
            if (!invertDash)
                buffer.append("=");
            else
                buffer.append("-");
        }

        for (int i = 0; i < tableWidth + ((tableWidth - 1) * horizontalSpacing) + 2; i++) {
            if (!invertDash)
                buffer.append("-");
            else
                buffer.append("=");
        }

        return buffer.toString();
    }

    /** For tables with a vertical header with no spaces in between: Builds a horizontal line.
     *
     * The division line has a different marker when it matches the same column as the vertical header.
     * A single header of numbers for example can have a width > 1. This requires 2 width without spacing since it's
     * only one element.
     *
     * @param tableWidth The length of the table (excluding spacing)
     * @param yHeaderWidth The vertical length of the header (spacing won't be added)
     * @param invertDash (0 = '-' first, '=' second, 1 = '=' first, '-' second)
     * @return A division line that matches the size of the table excluding spacing and the vertical header
     */
    private static String buildLineRowNoHeaderSpacing(int tableWidth, int yHeaderWidth, boolean invertDash){
        StringBuilder buffer = new StringBuilder();

        for (int i = 0; i < yHeaderWidth; i++) {
            if (!invertDash)
                buffer.append("=");
            else
                buffer.append("-");
        }

        for (int i = 0; i < tableWidth + ((tableWidth - 1) * horizontalSpacing) + 2; i++) {
            if (!invertDash)
                buffer.append("-");
            else
                buffer.append("=");
        }

        return buffer.toString();
    }

    /** For tables with no vertical header: Builds a horizontal line.
     *
     * The division line has a one marker when it matches the same column as the vertical header.
     * This marker can be changed using the boolean useDash
     *
     * @param tableWidth The length of the table (excluding spacing)
     * @param useDash Select the marker (0 = '-', 1 = '=')
     * @return A division line that matches the size of the table including spacing
     */
    private static String buildLineRow(int tableWidth, boolean useDash){
        StringBuilder buffer = new StringBuilder();
        char c;
        if (useDash)
            c = '-';
        else
            c = '=';

        for (int i = 0; i < tableWidth + ((tableWidth - 1) * horizontalSpacing) + 2; i++)
            buffer.append(c);

        return buffer.toString();
    }

    /** Builds the table with a vertical 2D header table and a border
     *
     * @param table The table that will be built (excluding spacing)
     * @param yHeader The vertical header prepended to each table row
     * @return A Table including spacing, a vertical 2D header and a border
     */
    private static List<String> buildTable(char[][] table, char[][] yHeader) {
        List<String> writeTable = new ArrayList<>();
        StringBuilder buffer;

        //table
        for (int i = 0; i < table.length; i++) {
            buffer = new StringBuilder();
            buffer.append(buildRow(yHeader[i]));
            buffer.append("|");
            buffer.append(buildRow(table[i]));
            buffer.append("|");
            writeTable.add(buffer.toString());
            if (i < table.length - 1) {
                for (int j = 0; j < verticalSpacing; j++) {
                    buffer = new StringBuilder();
                    for (int k = 0; k < yHeader[0].length + ((yHeader[0].length -1) * horizontalSpacing); k++){
                        buffer.append(" ");
                    }
                    buffer.append("|");
                    buffer.append(buildEmptyRow(table[0].length));
                    buffer.append("|");
                    writeTable.add(buffer.toString());
                }
            }
        }
        return writeTable;
    }

    /** Builds the table with a vertical header table and a border
     *
     * @param table The table that will be built (excluding spacing)
     * @param yHeader The vertical header prepended to each table row
     * @return A Table including spacing, a vertical header and a border
     */
    private static List<String> buildTable(char[][] table, char[] yHeader){
        List<String> writeTable = new ArrayList<>();
        StringBuilder buffer;

        //table
        for (int i = 0; i < table.length; i++) {
            buffer = new StringBuilder();
            buffer.append(yHeader[i]);
            buffer.append("|");
            buffer.append(buildRow(table[i]));
            buffer.append("|");
            writeTable.add(buffer.toString());
            if (i < table.length - 1) {
                for (int j = 0; j < verticalSpacing; j++) {
                    buffer = new StringBuilder();
                    buffer.append(" "); //Since the header is only 1 wide
                    buffer.append("|");
                    buffer.append(buildEmptyRow(table[0].length));
                    buffer.append("|");
                    writeTable.add(buffer.toString());
                }
            }
        }
        return writeTable;
    }

    /** Builds a table with no vertical header and a border
     *
     * @param table The table that will be built (excluding spacing)
     * @return A Table including spacing and borders
     */
    private static List<String> buildTable(char[][] table){
        List<String> writeTable = new ArrayList<>();
        StringBuilder buffer;

        //table
        for (int i = 0; i < table.length; i++){
            buffer = new StringBuilder();
            buffer.append("|");
            buffer.append(buildRow(table[i]));
            buffer.append("|");
            writeTable.add(buffer.toString());
            if (i < table.length - 1){
                for (int j = 0; j < verticalSpacing; j++) {
                    buffer = new StringBuilder();
                    buffer.append("|");
                    buffer.append(buildEmptyRow(table[0].length));
                    buffer.append("|");
                    writeTable.add(buffer.toString());
                }
            }
        }
        return writeTable;
    }

    /** Builds the table with a vertical header table and a border
     *
     * @param table The table that will be built (excluding spacing)
     * @param numbers The vertical number header prepended to each table row
     * @return A Table including spacing, a vertical header and a border
     */
    private static List<String> buildTable(char[][] table, int[] numbers){
        List<String> writeTable = new ArrayList<>();
        StringBuilder buffer;
        int maxDigitCount = getMaxDigitLength(numbers);

        //table
        for (int i = 0; i < table.length; i++) {
            buffer = new StringBuilder();
            int digitDifference = maxDigitCount - getDigitCount(numbers[i]);
            for (int k = 0; k < digitDifference; k++)
                buffer.append(" ");
            buffer.append(numbers[i]);
            buffer.append("|");
            buffer.append(buildRow(table[i]));
            buffer.append("|");
            writeTable.add(buffer.toString());
            if (i < table.length - 1) {
                for (int j = 0; j < verticalSpacing; j++) {
                    buffer = new StringBuilder();
                    buffer.append(" "); //Since the header is only 1 wide
                    buffer.append("|");
                    buffer.append(buildEmptyRow(table[0].length));
                    buffer.append("|");
                    writeTable.add(buffer.toString());
                }
            }
        }
        return writeTable;
    }

    /** Builds a horizontal header ontop (before building) the table.
     *
     * @param header The text that needs to be built (excluding spacing)
     * @return A horizontal header including text and spacing.
     */
    private static String buildHorizontalHeaderRow(char[] header){
        StringBuilder buffer = new StringBuilder();

        buffer.append("=");
        buffer.append(buildRow(header));
        buffer.append("=");

        return buffer.toString();
    }

    /** Builds a 2D horizontal header ontop (before building) the table.
     *
     * @param header The text that needs to be built (excluding spacing)
     * @return A 2D horizontal header including text and spacing.
     */
    private static List<String> buildHorizontalHeader(char[][] header){
        List<String> writeTable = new ArrayList<>();
        StringBuilder buffer;

        for(int row = 0; row < header[0].length; row++) {
            writeTable.add(buildHorizontalHeaderRow(header[row]));

            //Add whitelines if any.
            if (row < header[0].length -1) {
                for (int j = 0; j < verticalSpacing; j++) {
                    buffer = new StringBuilder();
                    buffer.append("=");
                    buffer.append(buildEmptyRow(header[1].length));
                    buffer.append("=");
                    writeTable.add(buffer.toString());
                }
            }
        }

        return writeTable;
    }

    /** Builds a horizontal header ontop (before building) the table.
     *
     * @param xheader The text that needs to be built (excluding spacing)
     * @param yHeaderWidth The offset due to the prepended vertical header
     * @return A horizontal header including text and spacing.
     */
    private static String buildHorizontalHeader(char[] xheader, int yHeaderWidth){
        StringBuilder buffer = new StringBuilder();

        for (int i = 0; i < yHeaderWidth + ((yHeaderWidth -1) * horizontalSpacing); i++)
            buffer.append(" ");
        buffer.append("=");
        buffer.append(buildRow(xheader));
        buffer.append("=");

        return buffer.toString();
    }

    /** Builds a 2D horizontal header ontop (before building) the table.
     *
     * @param header The text that needs to be built (excluding spacing)
     * @param yHeaderWidth The offset due to the prepended vertical header
     * @return A 2D horizontal header including text and spacing.
     */
    private static List<String> buildHorizontalHeader(char[][] header, int yHeaderWidth){
        List<String> writeTable = new ArrayList<>();
        StringBuilder buffer;

        for(int row = 0; row < header.length; row++) {
            writeTable.add(buildHorizontalHeader(header[row], yHeaderWidth));

            //Add whitelines if any.
            if (row < header.length - 1) {
                for (int j = 0; j < verticalSpacing; j++) {
                    buffer = new StringBuilder();
                    for (int k = 0; k < yHeaderWidth + ((yHeaderWidth -1) * horizontalSpacing); k++){
                        buffer.append(" ");
                    }
                    buffer.append("=");
                    buffer.append(buildEmptyRow(header[1].length));
                    buffer.append("=");
                    writeTable.add(buffer.toString());
                }
            }
        }

        return writeTable;
    }

    /** Builds a horizontal number header ontop (before building) the table.
     *
     * The spacing is dynamic once numbers are more than 1 digit in size.
     * The spacing will be the set spacing minus the excess digit amount.
     * E.g. Digit 10 will have 1 excess digit, so this will have one less space to match the other row chars.
     * Because any number has at least one digit, more than one will be counted as excess.
     * All chars are one in size.
     *
     * @param header The numbers that needs to be built (excluding spacing)
     * @return A horizontal number header including dynamic spacing.
     */
    private static String buildHorizontalHeader(int[] header){
        StringBuilder buffer = new StringBuilder();

        buffer.append("=");
        buffer.append(buildRow(header));
        buffer.append("=");

        return buffer.toString();
    }
//____________________________________________________________________________________________________________//
//
//											All Public Methods
//
//____________________________________________________________________________________________________________//

    /** Builds a table with no headers
     *
     * @param table The table with elements (excluding spaces)
     * @param tableName Printed above the table for information.
     * @param useDash Select top and bottom header marker (0 = '-', 1 = '=')
     * @return A 2D horizontal header including text and spacing.
     */
    public static List<String> basicTable(char[][] table, String tableName, boolean useDash){
        List<String> writeTable = new ArrayList<>();

        writeTable.add("");
        writeTable.add(tableName + " Table:");

        //Top row
        writeTable.add(buildLineRow(table[0].length, useDash));

        //table
        writeTable.addAll(buildTable(table));

        //Bottom row
        writeTable.add(buildLineRow(table[0].length, useDash));

        return writeTable;
    }

    /** Builds a table with a horizontal header
     *
     * @param table The table with elements (excluding spaces)
     * @param xHeader The horizontal header text above the table
     * @param tableName Printed above the table for information.
     * @return A table with a horizontal header (including spacing)
     */
    public static List<String> horizontalHeaderTable(char[][] table, char[] xHeader, String tableName){
        List<String> writeList = new ArrayList<>();

        writeList.add("");                      //Newline
        writeList.add(tableName + " table:");

        //Header
        writeList.add(buildHorizontalHeaderRow(xHeader));

        //Top row
        writeList.add(buildLineRow(table[0].length, false));

        //table
        writeList.addAll(buildTable(table));

        //bot row
        writeList.add(buildLineRow(table[0].length, true));

        return writeList;
    }

    /** Builds a table with a 2D horizontal header
     *
     * @param table The table with elements (excluding spaces)
     * @param xHeader The 2D horizontal header text above the table
     * @param tableName Printed above the table for information.
     * @return A table with a 2D horizontal header (including spacing)
     */
    public static List<String> horizontalHeaderTable(char[][] table, char[][] xHeader, String tableName){
        List<String> writeList = new ArrayList<>();

        writeList.add("");
        writeList.add(tableName + " table:");

        //Header
        writeList.addAll(buildHorizontalHeader(xHeader));

        //Top row
        writeList.add(buildLineRow(table[0].length, false));

        //table
        writeList.addAll(buildTable(table));

        //bot row
        writeList.add(buildLineRow(table[0].length, true));

        return writeList;
    }

    /** Builds a table with horizontal numbers
     *
     * @param table The table with elements (excluding spaces)
     * @param numbers The horizontal numbers with dynamic spacing above the table
     * @param tableName Printed above the table for information.
     * @return A table with horizontal numbers (including dynamic spacing)
     */
    public static List<String> horizontalHeaderTable(char[][] table, int[] numbers, String tableName){
        List<String> writeList = new ArrayList<>();

        int maxDigitLength = getMaxDigitLength(numbers);

        int horizontalSpacingPlaceholder = horizontalSpacing;
        horizontalSpacing+= maxDigitLength - 1; //-1 because a number always has at least 1 digit, so max -1 length is extra

        writeList.add("");                      //Newline
        writeList.add(tableName + " table:");

        //Number header
        writeList.add(buildHorizontalHeader(numbers));

        //Top row
        writeList.add(buildLineRow(table[0].length, false));

        //table
        writeList.addAll(buildTable(table));

        //bot row
        writeList.add(buildLineRow(table[0].length, true));

        horizontalSpacing = horizontalSpacingPlaceholder;

        return writeList;
    }

    /** Builds a table with a horizontal header and horizontal numbers
     *
     * @param table The table with elements (excluding spaces)
     * @param xHeader The horizontal header text above the table
     * @param numbers The horizontal numbers with dynamic spacing above the table
     * @param tableName Printed above the table for information.
     * @return A table with a horizontal header and horizontal numbers (including dynamic spacing)
     */
    public static List<String> horizontalHeaderTable(char[][] table, char[] xHeader, int[] numbers, String tableName){
        List<String> writeList = new ArrayList<>();

        int maxDigitLength = getMaxDigitLength(numbers);

        int horizontalSpacingPlaceholder = horizontalSpacing;
        horizontalSpacing+= maxDigitLength - 1; //-1 because a number always has at least 1 digit, so max -1 length is extra

        writeList.add("");                      //Newline
        writeList.add(tableName + " table:");

        //Header
        writeList.add(buildHorizontalHeaderRow(xHeader));

        //Number header
        writeList.add(buildHorizontalHeader(numbers));

        //Top row
        writeList.add(buildLineRow(table[0].length, false));

        //table
        writeList.addAll(buildTable(table));

        //bot row
        writeList.add(buildLineRow(table[0].length, true));

        horizontalSpacing = horizontalSpacingPlaceholder;

        return writeList;
    }

    /** Builds a table with a 2D horizontal header and horizontal numbers
     *
     * @param table The table with elements (excluding spaces)
     * @param xHeader The 2D horizontal header text above the table
     * @param numbers The horizontal numbers with dynamic spacing above the table
     * @param tableName Printed above the table for information.
     * @return A table with a 2D horizontal header and horizontal numbers (including dynamic spacing)
     */
    public static List<String> horizontalHeaderTable(char[][] table, char[][] xHeader, int[] numbers, String tableName){
        List<String> writeList = new ArrayList<>();

        int maxDigitLength = getMaxDigitLength(numbers);

        int horizontalSpacingPlaceholder = horizontalSpacing;
        horizontalSpacing+= maxDigitLength - 1; //-1 because a number always has at least 1 digit, so max -1 length is extra

        writeList.add("");                      //Newline
        writeList.add(tableName + " table:");

        //Header
        writeList.addAll(buildHorizontalHeader(xHeader));

        //Number header
        writeList.add(buildHorizontalHeader(numbers));

        //Top row
        writeList.add(buildLineRow(table[0].length, false));

        //table
        writeList.addAll(buildTable(table));

        //bot row
        writeList.add(buildLineRow(table[0].length, true));

        horizontalSpacing = horizontalSpacingPlaceholder;

        return writeList;
    }

    /** Builds a table with a 2D horizontal header and 2D horizontal numbers
     *
     * @param table The table with elements (excluding spaces)
     * @param xHeader The horizontal header text above the table
     * @param numbers The 2D horizontal numbers with dynamic spacing above the table
     * @param tableName Printed above the table for information.
     * @return A table with a horizontal header and 2D horizontal numbers (including dynamic spacing)
     */
    public static List<String> horizontalHeaderTable(char[][] table, char[] xHeader, int[][] numbers, String tableName){
        List<String> writeList = new ArrayList<>();

        int maxDigitLength = getMaxDigitLength(numbers);

        int horizontalSpacingPlaceholder = horizontalSpacing;
        horizontalSpacing+= maxDigitLength - 1; //-1 because a number always has at least 1 digit, so max -1 length is extra

        writeList.add("");                      //Newline
        writeList.add(tableName + " table:");

        //Header
        writeList.add(buildHorizontalHeaderRow(xHeader));

        //Number header
        for(int[]row : numbers)
            writeList.add(buildHorizontalHeader(row));

        //Top row
        writeList.add(buildLineRow(table[0].length, false));

        //table
        writeList.addAll(buildTable(table));

        //bot row
        writeList.add(buildLineRow(table[0].length, true));

        horizontalSpacing = horizontalSpacingPlaceholder;

        return writeList;
    }

    /** Builds a table with a 2D horizontal header and 2D horizontal numbers
     *
     * @param table The table with elements (excluding spaces)
     * @param xHeader The 2D horizontal header text above the table
     * @param numbers The 2D horizontal numbers with dynamic spacing above the table
     * @param tableName Printed above the table for information.
     * @return A table with a 2D horizontal header and 2D horizontal numbers (including dynamic spacing)
     */
    public static List<String> horizontalHeaderTable(char[][] table, char[][] xHeader, int[][] numbers, String tableName){
        List<String> writeList = new ArrayList<>();

        int maxDigitLength = getMaxDigitLength(numbers);

        int horizontalSpacingPlaceholder = horizontalSpacing;
        horizontalSpacing+= maxDigitLength - 1; //-1 because a number always has at least 1 digit, so max -1 length is extra

        writeList.add("");                      //Newline
        writeList.add(tableName + " table:");

        //Header
        writeList.addAll(buildHorizontalHeader(xHeader));

        //Number header
        for(int[]row : numbers)
            writeList.add(buildHorizontalHeader(row));

        //Top row
        writeList.add(buildLineRow(table[0].length, false));

        //table
        writeList.addAll(buildTable(table));

        //bot row
        writeList.add(buildLineRow(table[0].length, true));

        horizontalSpacing = horizontalSpacingPlaceholder;

        return writeList;
    }

    /** Builds a table with 2D horizontal numbers
     *
     * @param table The table with elements (excluding spaces)
     * @param numbers The 2D horizontal numbers with dynamic spacing above the table
     * @param tableName Printed above the table for information.
     * @return A table with 2D horizontal numbers (including dynamic spacing)
     */
    public static List<String> horizontalHeaderTable(char[][] table, int[][] numbers, String tableName){
        List<String> writeList = new ArrayList<>();

        int maxDigitLength = getMaxDigitLength(numbers);

        int horizontalSpacingPlaceholder = horizontalSpacing;
        horizontalSpacing+= maxDigitLength - 1; //-1 because a number always has at least 1 digit, so max -1 length is extra

        writeList.add("");                      //Newline
        writeList.add(tableName + " table:");

        //Number header
        for(int[]row : numbers)
            writeList.add(buildHorizontalHeader(row));

        //Top row
        writeList.add(buildLineRow(table[0].length, false));

        //table
        writeList.addAll(buildTable(table));

        //bot row
        writeList.add(buildLineRow(table[0].length, true));

        horizontalSpacing = horizontalSpacingPlaceholder;

        return writeList;
    }

    /** Builds a table with a vertical header
     *
     * @param table The table with elements (excluding spaces)
     * @param yHeader The vertical header text left of the table
     * @param tableName Printed above the table for information.
     * @return A table with a vertical header (including spacing)
     */
    public static List<String> verticalHeaderTable(char[][] table, char[] yHeader, String tableName){
        List<String> writeList = new ArrayList<>();
        int headerWidth = 1;

        writeList.add("");
        writeList.add(tableName + " table:");

        //Top row
        writeList.add(buildLineRow(table[0].length, headerWidth, false));

        //table
        writeList.addAll(buildTable(table, yHeader));

        //bot row
        writeList.add(buildLineRow(table[0].length, headerWidth,false));

        return writeList;
    }

    /** Builds a table with a 2D vertical header
     *
     * @param table The table with elements (excluding spaces)
     * @param yHeader The 2D vertical header text left of the table
     * @param tableName Printed above the table for information.
     * @return A table with a 2D vertical header (including spacing)
     */
    public static List<String> verticalHeaderTable(char[][] table, char[][] yHeader, String tableName){
        List<String> writeList = new ArrayList<>();
        int headerWidth = yHeader[1].length;

        writeList.add("");
        writeList.add(tableName + " table:");

        //Top row
        writeList.add(buildLineRow(table[0].length, headerWidth, true));

        //table
        writeList.addAll(buildTable(table, yHeader));

        //bot row
        writeList.add(buildLineRow(table[0].length, headerWidth, true));

        return writeList;
    }

    /** Builds a table with a vertical number header
     *
     * @param table The table with elements (excluding spaces)
     * @param numbers The vertical header numbers left of the table
     * @param tableName Printed above the table for information.
     * @return A table with a vertical number header (including spacing)
     */
    public static List<String> verticalHeaderTable(char[][] table, int[] numbers, String tableName){
        List<String> writeList = new ArrayList<>();
        int headerWidth = getMaxDigitLength(numbers);

        writeList.add("");
        writeList.add(tableName + " table:");

        //Top row
        writeList.add(buildLineRowNoHeaderSpacing(table[0].length, headerWidth, false));

        //table
        writeList.addAll(buildTable(table, numbers));

        //bot row
        writeList.add(buildLineRowNoHeaderSpacing(table[0].length, headerWidth,false));

        return writeList;
    }

    /** Builds a table with a horizontal and a vertical header
     *
     * @param table The table with elements (excluding spaces)
     * @param xHeader The horizontal header text above the table
     * @param yHeader The vertical header text left of the table
     * @param tableName Printed above the table for information.
     * @return A table with a horizontal and a vertical header (including spacing)
     */
    public static List<String> dualHeaderTable(char[][] table, char[] xHeader, char[] yHeader, String tableName){
        List<String> writeList = new ArrayList<>();
        int headerWidth = 1;

        writeList.add("");
        writeList.add(tableName + " table:");

        //Header
        writeList.add(buildHorizontalHeader(xHeader, headerWidth));

        //Top row
        writeList.add(buildLineRow(table[0].length, headerWidth, true));

        //table
        writeList.addAll(buildTable(table, yHeader));

        //bot row
        writeList.add(buildLineRow(table[0].length, headerWidth,true));

        return writeList;
    }

    /** Builds a table with a horizontal and a 2D vertical header
     *
     * @param table The table with elements (excluding spaces)
     * @param xHeader The horizontal header text above the table
     * @param yHeader The 2D vertical header text left of the table
     * @param tableName Printed above the table for information.
     * @return A table with a horizontal and a 2D vertical header (including spacing)
     */
    public static List<String> dualHeaderTable(char[][] table, char[] xHeader, char[][] yHeader, String tableName){
        List<String> writeList = new ArrayList<>();
        int headerWidth = yHeader[1].length;

        writeList.add("");
        writeList.add(tableName + " table:");

        //Header
        writeList.add(buildHorizontalHeader(xHeader, headerWidth));

        //Top row
        writeList.add(buildLineRow(table[0].length, headerWidth, true));

        //table
        writeList.addAll(buildTable(table, yHeader));

        //bot row
        writeList.add(buildLineRow(table[0].length, headerWidth, true));

        return writeList;
    }

    /** Builds a table with a 2D horizontal and a vertical header
     *
     * @param table The table with elements (excluding spaces)
     * @param xHeader The 2D horizontal header text above the table
     * @param yHeader The vertical header text left of the table
     * @param tableName Printed above the table for information.
     * @return A table with a 2D horizontal and a vertical header (including spacing)
     */
    public static List<String> dualHeaderTable(char[][] table, char[][] xHeader, char[] yHeader, String tableName){
        List<String> writeList = new ArrayList<>();
        int headerWidth = 1;

        writeList.add("");
        writeList.add(tableName + " table:");

        //Header
        writeList.addAll(buildHorizontalHeader(xHeader, headerWidth));

        //Top row
        writeList.add(buildLineRow(table[0].length, headerWidth, true));

        //table
        writeList.addAll(buildTable(table, yHeader));

        //bot row
        writeList.add(buildLineRow(table[0].length, headerWidth, true));

        return writeList;
    }

    /** Builds a table with a 2D horizontal and a 2D vertical header
     *
     * @param table The table with elements (excluding spaces)
     * @param xHeader The 2D horizontal header text above the table
     * @param yHeader The 2D vertical header text left of the table
     * @param tableName Printed above the table for information.
     * @return A table with a 2D horizontal and a 2D vertical header (including spacing)
     */
    public static List<String> dualHeaderTable(char[][] table, char[][] xHeader, char[][] yHeader, String tableName){
        List<String> writeList = new ArrayList<>();
        int headerWidth = yHeader[1].length;

        writeList.add("");
        writeList.add(tableName + " table:");

        //Header
        writeList.addAll(buildHorizontalHeader(xHeader, headerWidth));

        //Top row
        writeList.add(buildLineRow(table[0].length, headerWidth, true));

        //table
        writeList.addAll(buildTable(table, yHeader));

        //bot row
        writeList.add(buildLineRow(table[0].length, headerWidth,true));
        return writeList;
    }

}

package org.ciphers.Ciphers;

import org.userio.Blocking;
import org.logger.Logger;

//Use to read CIPHERNAME.txt in package 'Ciphers.InformationFiles' in 'setInformationFile()'
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import java.net.URL;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/** Forces every cipher to have an encrypt, a decrypt and a file-write method.
 * char[] encrypt(),
 * char[] decrypt(),
 * List<String> addInfoToOutputFile().
 */
interface CipherInterface {

	/** Method to set user IO variables.
	 *  This method is called when Encrypt() is chosen as action and after variable input is set.
	 *  This method can be seen as a constructor for when more user IO variables need to be set.
	 *  Separating these initializations with this method allows for unit tests.
	 */
	void initializeEncrypt();

	/** Method to set user IO variables.
	 *  This method is called when Encrypt() is chosen as action and after variable input is set.
	 *  This method can be seen as a constructor for when more user IO variables need to be set.
	 *  Separating these initializations with this method allows for unit tests.
	 */
	void initializeDecrypt();

	/** Uses protected char[] input to encrypt message.
	 * Once the user calls this method in the menu, the char[] input is forced to be set before this method is called.
	 * This variable is accessible.
	 *
	 * @return The encrypted message
	 */
	char[] encrypt();

	/** Uses protected char[] input to decrypt message.
	 * Once the user calls this method in the menu, the char[] input is forced to be set before this method is called.
	 * This variable is accessible.
	 *
	 * @return The decrypted message.
	 */
	char[] decrypt();

	/** (Optional) Add writing material for output.txt.
	 * Every Cipher writes CIPHERNAME and METHOD.
	 * When either the action 'Encrypt' or 'Decrypt' is called, INPUT and OUTPUT are also added.
	 * If more lines besides these are desired, they can be added using this method.
	 *
	 * @return A list of strings for the output.txt file, every String is a newline. Return null if not desired.
	 */
	List<String> writeAdditionalInfo();
}

public class Ciphers {

	static class ImpermissibleMethodCall extends Exception {
		public ImpermissibleMethodCall(String thisMethodName) {
			super("Action calls an inaccessible method.");
			Logger.LOGGER.warning("Action calls inaccessible method '" + thisMethodName + "'.");
		}
	}

	/** Stores the ran method in Iteration.java
	 * This data is used when all iterations are finished to retrieve all data for an overview.
	 */
	private Method selectedMethod;

	/** Stores the instance in Iteration.java
	 * This data is used when all iterations are finished to retrieve all data for an overview.
	 * This variable contains the fields used to retrieve data.
	 */
	private Object selectedInstance;

	/** Variable set by the user to encrypt or decrypt.
	 * Once the user calls this method in the menu, the char[] input is forced to be set before this method is called.
	 *
	 */
	protected char[] input;			//Available for all ciphers
	private char[] output;			//Return value of encrypt() or decrypt()

	/** (Optional) Adds method to the menu that prints String 'information' to the screen.
	 * This variable must be set in the cipher's constructor.
	 * Once this variable is not null, the method 'printInformation()' is added for this specific cipher.
	 * This method can be called by the user to print this string to the screen.
	 * ALTERNATIVE: A long description can be added instead by using String<List>> 'informationList'.
	 * To do this: add '<CIPHERNAME>.txt' to package 'Ciphers.InformationFiles'. If this file exists, it overrides
	 * this variable.
	 */
	private String information;

	/** (Optional) Adds method to the menu that prints List<String>'informationFile' to the screen.
	 * To do this: add '<CIPHERNAME>.txt' to package 'Ciphers.InformationFiles'.
	 * This file prioritizes over 'information'.
	 * ALTERNATIVE: A short description can be added instead by using String 'information'.
	 * This variable must be set in the cipher's constructor and <CIPHERNAME>.txt must not exist.
	 *
	 */
	private List<String> informationList;

//____________________________________________________________________________________________________________//
//												Methods
//												Public
//____________________________________________________________________________________________________________//

	/** Main method of class, Match method return type and calls coherent method
	 * If encrypt() or decrypt() is selected as method, this method runs first in order to set input.
	 * the output will call either encrypt() or decrypt().
	 * If any other method that doesn't return a char[] is called, this method is skipped and the selected
	 * child method is run instead.
	 *
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 */
	public void startCryption() throws InvocationTargetException, IllegalAccessException, ImpermissibleMethodCall {

		boolean isEncrypt = (selectedMethod.getName().equals("encrypt"));
		boolean isDecrypt = (selectedMethod.getName().equals("decrypt"));

		try {

			//Disallow calling this method from any other class and disallow calling itself from encrypt() or decrypt()
			if (!(isEncrypt || isDecrypt) && !isInvoked())
				throw new ImpermissibleMethodCall(getThisMethodName());

			input = setInput();

			if (isEncrypt)
				this.getClass().getDeclaredMethod("initializeEncrypt").invoke(selectedInstance);
			else if (isDecrypt)
				this.getClass().getDeclaredMethod("initializeDecrypt").invoke(selectedInstance);

			output = (char[]) selectedMethod.invoke(selectedInstance);
			Logger.LOGGER.info("'GET' Output: " + Arrays.toString(output));
			printOutput();    //Print in CMD during each iteration

		} catch (ImpermissibleMethodCall e){
			System.err.println(e.getMessage());

		} catch (NoSuchMethodException e) {
			throw new RuntimeException(e);
		}
	}


	/** Invoked by Write.java
	 * Extends output.txt with method specific information.
	 * This method is invoked by 'FileWrite'() in Write.java.
	 * If 'encrypt()' or 'Decrypt()' is called: write input and output.
	 * If cipher specific information is manually added in given cipher 'addInfoToOutputFile()' : append these lines.
	 *
	 * @return Write information of called method
	 * @throws NoSuchMethodException
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 */
	public List<String> setFileWriteMaterial() throws NoSuchMethodException, InvocationTargetException,
			IllegalAccessException {

		try {
			if (!isInvoked())
				throw new ImpermissibleMethodCall(getThisMethodName());

			List<String> list = new ArrayList<>();
			//Remove input output if an interface class isn't called
			if (selectedMethod.getName().equals("encrypt") ||
					selectedMethod.getName().equals("decrypt")) {
				StringBuilder buffer = new StringBuilder();

				list.add("");
				list.add("");
				list.add("Input:");
				for (char c : input)
					buffer.append(c);

				list.add(buffer.toString());
				buffer = new StringBuilder();

				list.add("");
				list.add("Output:");
				for (char c : output)
					buffer.append(c);

				list.add(buffer.toString());
			}

			//Attempt to add more information if listed in cipher specific parameter.
			//printInformation is a known method that does not need to print info
			if (!(selectedMethod.getName().equals("printInformation"))) {
				try {    //Add cipher specific info if applicable
					Method method = selectedInstance.getClass().getDeclaredMethod("writeAdditionalInfo");
					List<String> info = (List<String>) method.invoke(selectedInstance);
					if (info != null) {
						list.add("");
						list.addAll(info);
					}
				} catch (OutOfMemoryError e){
					String msg = "Failed writing additional info for Cipher: " +
							selectedInstance.getClass().getSimpleName() +
							" - Action: " + selectedMethod.getName();
					System.err.println(msg);
					Logger.LOGGER.warning(msg + ". Took too long to write and ran out of memory");

				} catch (Exception e){
					String msg = "Failed writing additional info for Cipher: " +
							selectedInstance.getClass().getSimpleName() +
							" - Action: " + selectedMethod.getName();
					System.err.println(msg);
					Logger.LOGGER.warning(msg + ". Perhaps a variable like 'input' is not initialized?");

				}
			}
			return list;

		} catch (ImpermissibleMethodCall e){
			System.err.println(e.getMessage());
			return null;
		}
	}

	/** Print 'information' or 'informationList' to the cmd.
	 *  Invoked by user.
	 * 'informationList' has priority over 'information' if both variables are set.
	 * To set informationList: Add <CIPHERNAME>.txt to package Ciphers/InformationFiles.
	 * To set information: Instantiate String in said cipher's constructor.
	 * If neither are instantiated, this method is excluded from the menu for said cipher.
	 * Print protected information string variable
	 * If no information exists, the printInformation option is excluded from the menu, thus the else statement will
	 * never be accessed.
	 */
	public void printInformation() {

		try {
			if (!isInvoked())
				throw new ImpermissibleMethodCall(getThisMethodName());

			System.out.print(System.lineSeparator());
			if (informationList != null)
				for (String line : informationList)
					System.out.println(line);
			else {
				if (information != null)
					System.out.println(information);
				else
					System.err.println("This Cipher does not have information listed");
			}
		} catch (ImpermissibleMethodCall e){
			System.err.println(e.getMessage());
		}
	}

	/** Reads Cipher information out of InformationFiles/CIPHERNAME.txt
	 * A textfile can be added for each cipher in package 'InformationFiles'.
	 * This file can be printed to the cmd with method 'PrintInformation'.
	 * Invoked in Menu.java 'execute()'
	 *
	 * @return (If File exists: returns 0, successful)
	 * @throws IOException
	 */
	public boolean setInformationFile() throws IOException, URISyntaxException {

		try {
			if (!isInvoked())
				throw new ImpermissibleMethodCall(getThisMethodName());

			String textFolder = "InformationFiles";
			ClassLoader loader = Thread.currentThread().getContextClassLoader();
			String cipherFile = textFolder + "/" + this.getClass().getSimpleName() + ".txt";
			int headerLength = 20; //Encapsulates text in CMD

			//Gives a NullPointerException if file does not exist.
			URL path = loader.getResource(cipherFile);
			URI uri = Objects.requireNonNull(path.toURI());

			informationList = new ArrayList<>();
			List<String> list = new ArrayList<>();

			BufferedReader reader = new BufferedReader(new InputStreamReader(uri.toURL().openStream()));

			String line;
			while ((line = reader.readLine()) != null)
				list.add(line);
			reader.close();

			informationList.add(buildHeader(headerLength));
			informationList.addAll(list);
			informationList.add(buildHeader(headerLength));

			return false;

		} catch (ImpermissibleMethodCall e) {
				System.err.println(e.getMessage());

		} catch (NullPointerException e) {
			//CIPHERNAME.txt does not exist in Ciphers/InformationFiles.
			//Skip setting informationList value.

		} catch (Exception ignored) {

		}
		return true;
	}


	/** Assign a number for each char in an array in their alphabetical order
	 * Used for columnar transposition
	 *
	 * @param arr The array that you want tog et the order from
	 * @return int array in order of the alphabetical order of the keyword
	 */
	public static int[] getAlphabeticalOrder(char[] arr) {
		int[] order = new int[arr.length];
		char[] sortedChars = Arrays.copyOf(arr, arr.length);
		Arrays.sort(sortedChars);

		//for each order spot
		int selectedOrderNumber = 0;
		//Compare ordered and non-ordered array
		for (char c : arr) {
			for (int j = 0; j < sortedChars.length; j++) {
				if (c == sortedChars[j]) {
					order[selectedOrderNumber] = j;
					selectedOrderNumber++;
					break;
				}
			}
		}
		return order;
	}

	/** Sort a char[] without changing the original array
	 *
	 * @param arr A sorted copy of parameter
	 * @return A sorted copy of parameter
	 */
	public static char[] sortArr(char[] arr){
		char[] sortedArr = Arrays.copyOf(arr, arr.length);
		Arrays.sort(sortedArr);
		return sortedArr;
	}

	/**
	 * Used at Write.java to get input of encryption/decryption
	 * @return User input for a cipher encrypt/decrypt action
	 */
	public char[] getInput(){
		return input;
	}

	/**
	 * Used at Write.java to get output of encryption/decryption
	 * @return User output for a cipher encrypt/decrypt action
	 */
	public char[] getOutput(){
		return output;
	}

	public boolean hasInformation(){
		return information != null || informationList != null;
	}

	public void setIteration(Object instance, Method method){

		try {
			if (!isInvoked())
				throw new ImpermissibleMethodCall(getThisMethodName());

			selectedInstance = instance;
			selectedMethod = method;
		} catch (ImpermissibleMethodCall e){
			System.err.println(e.getMessage());
		}
	}


//____________________________________________________________________________________________________________//
//												Methods
//												Protected
//____________________________________________________________________________________________________________//
	protected void setInformation(String msg){information = msg;}

	/** Set a specific keyword by user input
	 *
	 * @param keywordNumber An indicator which keyword will be returned, also used for user handling
	 * @return a keyword
	 */
	protected char[] setKeyword(int keywordNumber, boolean canBeEmpty){
		//System.out.print(System.lineSeparator());
		System.out.println("Keyword " + keywordNumber);
		char[] arr = Blocking.getString(canBeEmpty).toCharArray();

		Logger.LOGGER.info("'SET' Keyword ( " + keywordNumber + " ): " + Arrays.toString(arr));
		return arr;
	}

	/** Set a keyword by user input
	 *
	 * @param canBeEmpty If a char[] may be 0 characters "" or not.
	 * @return a keyword
	 */
	protected char[] setKeyword(boolean canBeEmpty){
		//System.out.print(System.lineSeparator());
		System.out.println("Keyword");
		char[] arr = Blocking.getString(canBeEmpty).toCharArray();

		Logger.LOGGER.info("'SET' Keyword: " + Arrays.toString(arr));
		return arr;
	}

	/** Set a key by user input
	 *
	 * @return a key as integer
	 */
	protected int setKey() {
		System.out.println("Key");
		int key = (Integer) Blocking.getNumber(Integer.class);

		Logger.LOGGER.info("'SET' Key: " + String.valueOf(key));
		return key;
	}

	/** Set a specific key by user input
	 *
	 * @return a key as integer
	 */
	protected int setKey(int keywordNumber) {
		System.out.println("Key");
		int key = (Integer) Blocking.getNumber(Integer.class);

		Logger.LOGGER.info("'SET' Key: " + String.valueOf(key));
		return key;
	}



//____________________________________________________________________________________________________________//
//												Methods
//												Private
//____________________________________________________________________________________________________________//

	/** Adds header to encapsulate informationFile
	 *
	 * @param length Length of the header
	 * @return String of header
	 */
	private String buildHeader(int length){
		StringBuilder buffer = new StringBuilder();
		for (int i = 0; i < length; i++)
			buffer.append("=");
		return buffer.toString();
	}

	private char[] setInput() {
		System.out.print(System.lineSeparator());
		System.out.println("input");
		char[] arr = Blocking.getString(false).toCharArray();

		Logger.LOGGER.info("'SET' Input: " + Arrays.toString(arr));
		return arr;

	}

	private void printOutput() {
		//System.out.print(System.lineSeparator());
		System.out.println("Output:");
		System.out.println(output);
	}

	/** Checks if the method that calls this method had been called or invoked.
	 * This method = 0, The method in question = 1, The method that called method 1 = 2.
	 * @return True if invoked, False if called.
	 */
	private boolean isInvoked(){
		int layerOfMethod = 2;
		return new Exception().getStackTrace()[layerOfMethod].getMethodName().startsWith("invoke");
	}

	/** Get the name of the method calling this.
	 * @return Method name as String
	 */
	private String getThisMethodName(){
		return new Exception().getStackTrace()[1].getMethodName();
	}
}
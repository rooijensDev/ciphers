package org.ciphers;

import java.util.logging.Level;

import org.userio.Exceptions.InvalidScope;
import org.logger.Logger;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;

public class Main {
	private static final String version = "0.4.0";
	private static final String licenseNotice = "Copyright (c) 2020-2024, Ramon Lorenzo Rooijens. All rights reserved.";
	private static final String website = "https://github.com/RamonLorenRooijens/ciphers";	

	public static void main(String[] args) throws InvocationTargetException, NoSuchMethodException, InstantiationException,
			IllegalAccessException, InvalidScope, IOException, URISyntaxException, ClassNotFoundException, InterruptedException {

		try {
			System.out.println(licenseNotice);
			System.out.println(website);
			System.out.println("Version: " + version);
			
			Logger.start("ciphers-log.md", Level.ALL, Main.class);
			Logger.LOGGER.info(version);

			Menu menu = new Menu();
			menu.cipherMenu();

		} finally {
				endProgramCMD();
			}
	}

	/** Avoid abrupt CMD terminate for the end-user.
	 * @throws InterruptedException Sleep method
	 */
	private static void endProgramCMD() throws InterruptedException {
		Logger.LOGGER.info("Exit program");
		System.out.print(System.lineSeparator());
		System.out.print("Exiting program");

		int intervalInMillis = 500;

		for (int i = 0; i < 3; i++) {
			Thread.sleep(intervalInMillis);
			System.out.print(".");
		}
		Thread.sleep(intervalInMillis);
	}
}
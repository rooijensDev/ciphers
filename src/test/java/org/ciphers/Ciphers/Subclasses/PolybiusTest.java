package org.ciphers.Ciphers.Subclasses;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class PolybiusTest {

//____________________________________________________________________________________________________________//
//
//											    Methods
//
//____________________________________________________________________________________________________________//

    @ParameterizedTest
    @MethodSource("params_polybius6x6")
    void method_polybius6x6(String keyword, char[][] expectedOutput) {
        char[][] output = Polybius.polybiusSquare6x6(keyword.toCharArray());

        printExpectedAndActualOutputs(expectedOutput, output);

        assertArrayEquals(output, expectedOutput);
    }

    @ParameterizedTest
    @MethodSource("params_polybius5x5_default")
    void method_polybius5x5_default(String keyword, char[][] expectedOutput) {
        char[][] output = Polybius.polybiusSquare5x5(keyword.toCharArray());

        printExpectedAndActualOutputs(expectedOutput, output);

        assertArrayEquals(output, expectedOutput);
    }

    @ParameterizedTest
    @MethodSource("params_polybius5x5_customLetterReplacement")
    void method_polybius5x5_customLetterReplacement(String keyword, char letterRemoved, char letterReplacement, char[][] expectedOutput) {
        char[][] output = Polybius.polybiusSquare5x5(keyword.toCharArray(), letterRemoved, letterReplacement);

        printExpectedAndActualOutputs(expectedOutput, letterRemoved, letterReplacement, output);

        assertArrayEquals(output, expectedOutput);
    }

    private void printExpectedAndActualOutputs(char[][] expected, char[][] actual){
        System.out.println("Expected output: ");
        for (char[]row: expected)
            System.out.println(Arrays.toString((row)));

        System.out.print(System.lineSeparator());

        System.out.println("Actual output: ");
        for (char[]row: actual)
            System.out.println(Arrays.toString((row)));
    }

    private void printExpectedAndActualOutputs(char[][] expected, char letterRemoved, char letterReplacement, char[][] actual){
        System.out.println("Letter '" + letterRemoved + "' is replaced by '" + letterReplacement + "'");
        System.out.print(System.lineSeparator());

        printExpectedAndActualOutputs(expected, actual);
    }


//____________________________________________________________________________________________________________//
//
//											    Params
//
//____________________________________________________________________________________________________________//

    private static Stream<Arguments> params_polybius6x6() {
        char[][] square_EMPTYKEYWORD = new char[][]{
                {'A', '1', 'B', '2', 'C', '3'},
                {'D', '4', 'E', '5', 'F', '6'},
                {'G', '7', 'H', '8', 'I', '9'},
                {'J', '0', 'K', 'L', 'M', 'N'},
                {'O', 'P', 'Q', 'R', 'S', 'T'},
                {'U', 'V', 'W', 'X', 'Y', 'Z'}
        };

        char[][] square_test = new char[][]{
                {'T', 'E', '5', 'S', 'A', '1'},
                {'B', '2', 'C', '3', 'D', '4'},
                {'F', '6', 'G', '7', 'H', '8'},
                {'I', '9', 'J', '0', 'K', 'L'},
                {'M', 'N', 'O', 'P', 'Q', 'R'},
                {'U', 'V', 'W', 'X', 'Y', 'Z'}
        };

        char[][] square_stokbroodje = new char[][]{
                {'S', 'T', 'O', 'K', 'B', '2'},
                {'R', 'D', '4', 'J', '0', 'E'},
                {'5', 'A', '1', 'C', '3', 'F'},
                {'6', 'G', '7', 'H', '8', 'I'},
                {'9', 'L', 'M', 'N', 'P', 'Q'},
                {'U', 'V', 'W', 'X', 'Y', 'Z'}
        };

        return Stream.of(
                arguments("", square_EMPTYKEYWORD),
                arguments("test", square_test),
                arguments("stokbroodje", square_stokbroodje)

        );
    }

    private static Stream<Arguments> params_polybius5x5_default() {
        char[][] square_EMPTYKEYWORD = new char[][]{
                {'A', 'B', 'C', 'D', 'E'},
                {'F', 'G', 'H', 'I', 'K'},
                {'L', 'M', 'N', 'O', 'P'},
                {'Q', 'R', 'S', 'T', 'U'},
                {'V', 'W', 'X', 'Y', 'Z'}
        };


        char[][] square_stokbroodje = new char[][]{
                {'S', 'T', 'O', 'K', 'B'},
                {'R', 'D', 'I', 'E', 'A'},
                {'C', 'F', 'G', 'H', 'L'},
                {'M', 'N', 'P', 'Q', 'U'},
                {'V', 'W', 'X', 'Y', 'Z'}
        };

        return Stream.of(
                arguments("", square_EMPTYKEYWORD),
                arguments("stokbroodje", square_stokbroodje)

        );
    }

    private static Stream<Arguments> params_polybius5x5_customLetterReplacement() {
        char[][] square_EMPTYKEYWORD_replace_a_with_z = new char[][]{
                {'Z', 'B', 'C', 'D', 'E'},
                {'F', 'G', 'H', 'I', 'J'},
                {'K', 'L', 'M', 'N', 'O'},
                {'P', 'Q', 'R', 'S', 'T'},
                {'U', 'V', 'W', 'X', 'Y'}
        };

        char[][] square_test_replace_e_with_i = new char[][]{
                {'T', 'I', 'S', 'A', 'B'},
                {'C', 'D', 'F', 'G', 'H'},
                {'J', 'K', 'L', 'M', 'N'},
                {'O', 'P', 'Q', 'R', 'U'},
                {'V', 'W', 'X', 'Y', 'Z'}
        };

        char[][] square_stokbroodje_replace_a_with_z = new char[][]{
                {'S', 'T', 'O', 'K', 'B'},
                {'R', 'D', 'J', 'E', 'Z'},
                {'C', 'F', 'G', 'H', 'I'},
                {'L', 'M', 'N', 'P', 'Q'},
                {'U', 'V', 'W', 'X', 'Y'}
        };

        return Stream.of(
                arguments("", 'A', 'Z', square_EMPTYKEYWORD_replace_a_with_z),
                arguments("12345", 'A', 'Z', square_EMPTYKEYWORD_replace_a_with_z),
                arguments("stokbroodje", 'A', 'Z', square_stokbroodje_replace_a_with_z),
                arguments("test", 'E', 'I', square_test_replace_e_with_i)

        );
    }
}
package org.ciphers.Ciphers;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.userio.Filter;

import java.util.Arrays;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class AdfgvxTest {

//____________________________________________________________________________________________________________//
//
//											    Methods
//
//____________________________________________________________________________________________________________//

    @ParameterizedTest
    @MethodSource("params_encryptPass")
    void encryptPass(String input, String keyword1, String keyword2, String expectedOutput) {
        Adfgvx instance = new Adfgvx();
        instance.input = input.toCharArray();
        instance.originalKeywords = new char[][]{keyword1.toCharArray(), keyword2.toCharArray()};

        char[] output = instance.encrypt();

        System.out.println("Expected Output: " + Arrays.toString(expectedOutput.toCharArray()));
        System.out.println("Actual Output:   " + Arrays.toString(output));

        assertArrayEquals(output, expectedOutput.toCharArray());
    }

    @ParameterizedTest
    @MethodSource("params_decryptPass")
    void decryptPass(String input, String keyword1, String keyword2, String expectedOutput) {
        Adfgvx instance = new Adfgvx();
        instance.input = input.toCharArray();
        instance.originalKeywords = new char[][]{keyword1.toCharArray(), keyword2.toCharArray()};

        char[] output = instance.decrypt();

        System.out.println("Expected Output: " + Arrays.toString(expectedOutput.toCharArray()));
        System.out.println("Actual Output:   " + Arrays.toString(output));

        assertArrayEquals(output, expectedOutput.toCharArray());
    }

//____________________________________________________________________________________________________________//
//
//											    Params
//
//____________________________________________________________________________________________________________//

    private static Stream<Arguments> params_encryptPass() {
        return Stream.of(
                arguments("This is a secret message!", "Stokbroodje", "Kruidenboter", "AXFDG AAFXA DDGDX AAADA ADVGG FDDDA AAXGX XGFDA"),
                arguments("hello", "Stokbroodje", "Kruidenboter", "DVDXG VAGFD"),
                arguments("hello", "STOKStokbroodje", "Kruidenboter", "DVDXG VAGFD"),
                arguments("hello987", "STOKStokbroodje", "Kruidenboter", "DVGDF XVGVV AGAFD G"),
                arguments("hello987...", "STOKStokbroodje", "Kruidenboter", "DVGDF XVGVV AGAFD G"),
                arguments("hello987...", "123STOK9999987Stokbroodje", "Kruiden654boter", "DVDDG VFGDV AXDXF D")

        );
    }

    private static Stream<Arguments> params_decryptPass() {
        return Stream.of(
                arguments("AXFDG AAFXA DDGDX AAADA ADVGG FDDDA AAXGX XGFDA", "Stokbroodje", "Kruidenboter", decryptFilter("This is a secret message")),
                arguments("DVDXG VAGFD", "Stokbroodje", "Kruidenboter", decryptFilter("hello")),
                arguments("DVDXG VAGFD", "STOKStokbroodje", "Kruidenboter", decryptFilter("hello")),
                arguments("DVGDF XVGVV AGAFD G", "STOKStokbroodje", "Kruidenboter", decryptFilter("hello987")),
                arguments("DVGDF XVGVV AGAFD G", "STOKStokbroodje", "Kruidenboter", decryptFilter("hello987...")),
                arguments("DVDDG VFGDV AXDXF D", "123STOK9999987Stokbroodje", "Kruiden654boter", decryptFilter("hello987..."))

        );
    }

    private static String decryptFilter(String s){
        return Filter.onlyLettersAndNumbers(
                    Filter.capitalize(s, true));
    }
}
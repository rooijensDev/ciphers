package org.ciphers.Ciphers;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class PortaTest {

//____________________________________________________________________________________________________________//
//
//											    Methods
//
//____________________________________________________________________________________________________________//

    @ParameterizedTest
    @MethodSource("dataEncryptExpectPass")
    void encryptPass(String input, String keyword, String expectedOutput) {
        Porta instance = new Porta();
        instance.input = input.toCharArray();
        instance.keyword = keyword.toCharArray();

        assertArrayEquals(instance.encrypt(), expectedOutput.toCharArray());
    }

    @ParameterizedTest
    @MethodSource("dataDecryptExpectPass")
    void decryptPass(String input, String keyword, String expectedOutput) {
        Porta instance = new Porta();
        instance.input = input.toCharArray();
        instance.keyword = keyword.toCharArray();

        assertArrayEquals(instance.decrypt(), expectedOutput.toCharArray());
    }

//____________________________________________________________________________________________________________//
//
//											    Params
//
//____________________________________________________________________________________________________________//

    private static Stream<Arguments> dataEncryptExpectPass() {
        return Stream.of(
                arguments("This is a secret message!", "Stokbroodje", "KQPAVKULSTCNKTWFKUNS")
        );
    }

    private static Stream<Arguments> dataDecryptExpectPass() {
        return Stream.of(
                arguments("KQPAVKULSTCNKTWFKUNS", "Stokbroodje", "THISISASECRETMESSAGE")
        );
    }
}
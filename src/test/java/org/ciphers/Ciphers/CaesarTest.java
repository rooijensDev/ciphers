package org.ciphers.Ciphers;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import java.util.stream.Stream;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class CaesarTest {

//____________________________________________________________________________________________________________//
//
//											    Methods
//
//____________________________________________________________________________________________________________//

    @ParameterizedTest
    @MethodSource("params_data")
    void encryptPass(String input, int shift, String expectedOutput) {
        Caesar instance = new Caesar();
        instance.shift = shift;
        instance.input = input.toCharArray();

        assertArrayEquals(instance.encrypt(), expectedOutput.toCharArray());
    }

    @ParameterizedTest
    @MethodSource("params_data")
    void decryptPass(String input, int shift, String expectedOutput) {
        Caesar instance = new Caesar();
        instance.shift = shift;
        instance.input = input.toCharArray();

        assertArrayEquals(instance.decrypt(), expectedOutput.toCharArray());
    }

//____________________________________________________________________________________________________________//
//
//											    Params
//
//____________________________________________________________________________________________________________//

    private static Stream<Arguments> params_data() {
        return Stream.of(
                arguments("abc", 1, "bcd"),
                arguments("abc", -1, "zab"),
                arguments("abc", 26, "abc"),
                arguments("hello world!", 8, "pmttw ewztl!"),
                arguments("hello world!", -18, "pmttw ewztl!"),
                arguments("hello world!", 34, "pmttw ewztl!")
        );
    }
}
package org.ciphers.Ciphers;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import java.util.stream.Stream;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class Rot13Test {

//____________________________________________________________________________________________________________//
//
//											    Methods
//
//____________________________________________________________________________________________________________//

    @ParameterizedTest
    @MethodSource("params_data")
    void encryptPass(String input, String expectedOutput) {
        Rot13 instance = new Rot13();
        instance.input = input.toCharArray();

        assertArrayEquals(instance.encrypt(), expectedOutput.toCharArray());
    }

    @ParameterizedTest
    @MethodSource("params_data")
    void decryptPass(String input, String expectedOutput) {
        Rot13 instance = new Rot13();
        instance.input = input.toCharArray();

        assertArrayEquals(instance.encrypt(), expectedOutput.toCharArray());
    }

//____________________________________________________________________________________________________________//
//
//											    Params
//
//____________________________________________________________________________________________________________//

    private static Stream<Arguments> params_data() {
        return Stream.of(
                arguments("abc", "nop")
        );
    }
}
# Encrypt and Decrypt a number of ciphers
Project repository found on [Git](https://gitlab.com/rooijensDev/ciphers).

Found a bug or want to request a feature?\
Please make a ticket on the [Issue Page](https://gitlab.com/rooijensDev/ciphers/-/issues)
or email me at _rooijensDev@gmail.com_\
It is kindly appreciated if you report any bugs you find.

### Available ciphers:
* Caesar
* Adfgvx
* Porta
* Rot13

## Instructions
### User
1. Make sure to have [Java](https://www.java.com/en/download/) installed (Version 8 or higher).
   * You can check with `java -version` in the command prompt.
2. Download the package asset under [_Releases_](https://gitlab.com/rooijensDev/ciphers/-/releases/).
2. Run the batch file

### Developer

To include the required submodule 'basicUserIO', make sure to include it in your clone command.
<br/> `git clone --recurse-submodules --remote-submodules https://gitlab.com/rooijensDev/ciphers`
Add _lib/basicUserIO/_**basicUserIO.jar** to the libraries in your project structure if this
has not been done automatically yet. If this does not work, check if it is also added as a dependency.

#### A **Guide** on how to add your own ciphers can be found on the [Wiki](https://gitlab.com/rooijensDev/ciphers/-/wikis/home).

# Add your own ciphers with ease!
**This code works dynamic,**
Each cipher will automatically be added to the main program.
The only thing you have to do, is to create a java file
within the *Ciphers* Package with the following base code:<br>

*Note:* You can also **create your own executable methods** by simply making it *public*.

```
package Ciphers;

import java.util.List;

public class CIPHERNAME extends Ciphers implements CipherInterface {
    //You may use the already initialized char[] 'input'

    public CIPHERNAME(){
        information =   "You can add information about this cipher here, " +
                "this is not necessary and can be left out";
    }

    @Override
    public char[] encrypt() {
        return aCharArray;
    }

    @Override
    public char[] decrypt() {
        return aCharArray;
    }
    
    //You can give the file writer more write material
    //Each String listed will be a newline in the text file
    @Override
    public List<String> addInfoToOutputFile() {
        return null;
    }
}
```
